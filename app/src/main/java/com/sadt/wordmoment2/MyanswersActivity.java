package com.sadt.wordmoment2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import com.sadt.wordmoment2.Deck.CardAdapter;


public class MyanswersActivity extends ActionBarActivity {
    private ListView myanswers_listview;
    private CardAdapter adapter;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myanswers);

        myanswers_listview = (ListView) findViewById(R.id.myanswers_listview);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        adapter = new CardAdapter(this, 0, PlayingActivity.cards, PlayingActivity.answers, false);
        myanswers_listview.setAdapter(adapter);
    }

    public void back(View view) {
        finish();
    }

}
