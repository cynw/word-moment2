package com.sadt.wordmoment2.Flashcard;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.Deck.DeckActivity;
import com.sadt.wordmoment2.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class FlashcardActivity extends AppCompatActivity implements TextToSpeech.OnInitListener {
    private Toolbar toolbar;
    private static Context context;
    private List<Card> cards;
    private int lastIndex, deckId, size;
    private TextView word, flashcard_count;
    private Deck deck;
    private String forLan, lanCode;
    private boolean local = false;
    public static boolean baselocal = false;
    private TextToSpeech tts;
    private MediaPlayer mediaPlayer;
    private boolean ttsReadable = false;
    private ArrayList<Integer> hideIndex;
    public static Boolean day;
    private Button hide_btn;
    private RelativeLayout flashcard_lo, card_lo;
    private Menu menu;
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flashcard);
        hideIndex = new ArrayList<>();
        context = this;

        day = true;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        flashcard_lo = (RelativeLayout) findViewById(R.id.flashcard_lo);
        //card_lo = (RelativeLayout) findViewById(R.id.card_lo);
        //word = (TextView) findViewById(R.id.word);
        flashcard_count = (TextView) findViewById(R.id.flashcard_count);
        hide_btn = (Button) findViewById(R.id.hide_btn);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        deck = DeckActivity.deck;
        cards = DeckActivity.cards;

        if(deck.getDeckName2()==null) getSupportActionBar().setTitle(deck.getDeckName());
        else getSupportActionBar().setTitle(deck.getDeckName()+" "+deck.getDeckName2());

        Intent intent = getIntent();
        deckId = intent.getIntExtra("deckId", -1);
        forLan = intent.getStringExtra("forLan");
        baselocal = intent.getBooleanExtra("baselocal", false);

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        checkIntent.setPackage("com.google.android.tts");
        startActivityForResult(checkIntent, 0);

        size = cards.size();
        lastIndex = deck.getLastIndex();

        flashcard_count.setText(String.valueOf(lastIndex) + " / " + String.valueOf(size));

        if (size == 0)
            finish();

        hide_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hide_session();
            }
        });

        hide_btn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                hide_today();
                return false;
            }
        });

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                lastIndex = position;
                flashcard_count.setText(String.valueOf(lastIndex) + " / " + String.valueOf(size));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        /*if (baselocal) {
            word.setText(cards.get(lastIndex).getLocal());
            word.setTextColor(getResources().getColor(R.color.ColorPrimaryBridght));
        }
        else {
            word.setText(cards.get(lastIndex).getForeign());
            word.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.flashcard, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.day_night:
                if (day){
                    day = false;
                    flashcard_lo.setBackgroundResource(R.color.flashcard_night_bg);
                    hide_btn.setBackgroundResource(R.drawable.hide_night);
                    hide_btn.setTextColor(getResources().getColor(R.color.white));
                    //word.setTextColor(getResources().getColor(R.color.white));
                    //card_lo.setBackgroundResource(R.drawable.card_night)
                    flashcard_count.setTextColor(getResources().getColor(R.color.white));
                    menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.day));
                    mPagerAdapter.notifyDataSetChanged();
                }
                else{
                    day = true;
                    flashcard_lo.setBackgroundResource(R.color.flashcard_day_bg);
                    hide_btn.setBackgroundResource(R.drawable.hide_day);
                    hide_btn.setTextColor(getResources().getColor(R.color.ColorPrimary));
                    //card_lo.setBackgroundResource(R.drawable.card_day);
                    flashcard_count.setTextColor(getResources().getColor(R.color.ColorPrimary));
                    menu.getItem(0).setIcon(getResources().getDrawable(R.drawable.night));
                    mPagerAdapter.notifyDataSetChanged();
                }
                return super.onOptionsItemSelected(item);
            case R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                //tts = new TextToSpeech(this, this);
                tts = new TextToSpeech(this, this, "com.google.android.tts");
            } else {
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                installIntent.setPackage("com.google.android.tts");
                startActivity(installIntent);
            }
        }
    }

    protected void onStop() {
        super.onStop();
        deck.setLastIndex(lastIndex);
        deck.saveEventually();
    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onDestroy();
    }


    public void prev(View view) {
        do {
            if (lastIndex == 0)
                lastIndex = size - 1;
            else
                lastIndex--;
        } while (hideIndex.contains(lastIndex));

        flashcard_count.setText(String.valueOf(lastIndex) + " / " + String.valueOf(size));

        if (baselocal) {
            word.setText(cards.get(lastIndex).getLocal());
            if(day) word.setTextColor(getResources().getColor(R.color.ColorPrimaryBridght));
        }
        else {
            word.setText(cards.get(lastIndex).getForeign());
            if(day) word.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
    }

    public void next(View view) {
        do {
            if (lastIndex == size - 1)
                lastIndex = 0;
            else
                lastIndex++;
        } while (hideIndex.contains(lastIndex));

        flashcard_count.setText(String.valueOf(lastIndex) + " / " + String.valueOf(size));

        if (baselocal) {
            word.setText(cards.get(lastIndex).getLocal());
            if(day) word.setTextColor(getResources().getColor(R.color.ColorPrimaryBridght));
        }
        else {
            word.setText(cards.get(lastIndex).getForeign());
            if(day) word.setTextColor(getResources().getColor(R.color.ColorPrimary));
        }
    }

    public void flip(View view) {
        if (local) {
            word.setText(cards.get(lastIndex).getForeign());
            if(day) word.setTextColor(getResources().getColor(R.color.ColorPrimary));
            local = false;
        } else {
            word.setText(cards.get(lastIndex).getLocal());
            if(day) word.setTextColor(getResources().getColor(R.color.ColorPrimaryBridght));
            local = true;
        }
    }

    /*public void delete(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_card_msg)
                .setTitle(R.string.delete_card_title)
                .setPositiveButton(R.string.delete_warning_agree, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        cards.get(lastIndex).deleteInBackground();
                        cards.remove(lastIndex);
                        size--;
                        Intent intent = new Intent();
                        if (size == 0) {
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            if (baselocal)
                                word.setText(cards.get(lastIndex).getLocal());
                            else
                                word.setText(cards.get(lastIndex).getForeign());
                            setResult(RESULT_OK, intent);
                        }
                    }
                })
                .setNegativeButton(R.string.delete_warning_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }*/

    public void hide_session() {
        hideIndex.add(lastIndex);
        Log.d("Hideindex", String.valueOf(hideIndex.size()));
        Log.d("Cards", String.valueOf(size));
        if (hideIndex.size() == size) {
            Toast.makeText(context, "No more words in this session", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void hide_today() {
        Card card = cards.get(lastIndex);
        Date today = new Date();
        Date tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24));
        card.setDisplay(tomorrow);
        card.saveEventually();
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        hide_session();
    }

    public void read(View view) {
        String text = cards.get(lastIndex).getForeign();
        if (ttsReadable) {
            Log.d("Reading by TTS", text);
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else {
            try {
                String url = "http://translate.google.com/translate_tts";
                url += "?ie=UTF-8&tl=";
                url += lanCode;
                url += "&q=";
                url += text;
                mediaPlayer = new MediaPlayer();
                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                mediaPlayer.setDataSource(url);
                mediaPlayer.prepare();
                mediaPlayer.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onInit(int status) {
        lanCode = forLan.substring(0, 2).toLowerCase();
        Locale local = new Locale(lanCode);
        if (tts.isLanguageAvailable(local) >= 0) {
            if (status == TextToSpeech.SUCCESS) {
                tts.setLanguage(local);
                tts.setSpeechRate(0.5f);
                ttsReadable = true;
            } else {
                Log.e("TTS", "Initilization Failed!");
            }
        }
    }

    private class ScreenSlidePagerAdapter extends FragmentPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
            fragment.setCard(cards.get(position));
            return fragment;
        }

        @Override
        public int getCount() {
            return cards.size();
        }
    }
}
