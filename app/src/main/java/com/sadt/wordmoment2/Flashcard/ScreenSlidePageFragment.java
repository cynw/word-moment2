/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.sadt.wordmoment2.Flashcard;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.R;


public class ScreenSlidePageFragment extends Fragment{
    private boolean cardFlipped = false;
    private Card card;

    public ScreenSlidePageFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_screen_slide_page, container, false);

        FrameLayout container_fl = (FrameLayout) rootView.findViewById(R.id.container);

        container_fl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flipCard();
            }
        });

        CardFrontFragment cbfragment = new CardFrontFragment();
        cbfragment.setCard(card);
        getChildFragmentManager()
                .beginTransaction()
                .add(R.id.container, cbfragment)
                .commit();

        return rootView;
    }

    public void setCard(Card mcard){
        this.card = mcard;
    }


    public void flipCard() {
        if (cardFlipped) {
            CardFrontFragment cffragmnet = new CardFrontFragment();
            cffragmnet.setCard(card);
            getChildFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            R.anim.card_flip_right_in,
                            R.anim.card_flip_right_out,
                            R.anim.card_flip_left_in,
                            R.anim.card_flip_left_out)
                    .replace(R.id.container, cffragmnet)
                    .commit();
        } else {
            CardBackFragment cbfragment = new CardBackFragment();
            cbfragment.setCard(card);
            getChildFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(
                            R.anim.card_flip_right_in,
                            R.anim.card_flip_right_out,
                            R.anim.card_flip_left_in,
                            R.anim.card_flip_left_out)
                    .replace(R.id.container, cbfragment)
                    .commit();
        }

        cardFlipped = !cardFlipped;
    }

    public static class CardFrontFragment extends Fragment {
        private Card card;
        public CardFrontFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_card_front, container, false);
            TextView word_tv = (TextView) v.findViewById(R.id.word);
            RelativeLayout flashcard_lo = (RelativeLayout) v.findViewById(R.id.flashcard_lo);

            if (FlashcardActivity.baselocal) word_tv.setText(card.getLocal());
            else word_tv.setText(card.getForeign());

            if(FlashcardActivity.day){
                word_tv.setTextColor(getResources().getColor(R.color.ColorPrimary));
                flashcard_lo.setBackgroundResource(R.drawable.card_day);
            }
            else{
                word_tv.setTextColor(getResources().getColor(R.color.white));
                flashcard_lo.setBackgroundResource(R.drawable.card_night);
            }

            return v;
        }

        public void setCard(Card mcard){
            card = mcard;
        }

    }

    /**
     * A fragment representing the back of the card.
     */
    public static class CardBackFragment extends Fragment {
        private Card card;
        public CardBackFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.fragment_card_back, container, false);
            TextView word_tv = (TextView) v.findViewById(R.id.word);
            RelativeLayout flashcard_lo = (RelativeLayout) v.findViewById(R.id.flashcard_lo);
            if (FlashcardActivity.baselocal) word_tv.setText(card.getForeign());
            else word_tv.setText(card.getLocal());

            if(FlashcardActivity.day){
                word_tv.setTextColor(getResources().getColor(R.color.ColorPrimaryBridght));
                flashcard_lo.setBackgroundResource(R.drawable.card_day);
            }
            else{
                word_tv.setTextColor(getResources().getColor(R.color.white));
                flashcard_lo.setBackgroundResource(R.drawable.card_night);
            }

            return v;
        }

        public void setCard(Card mcard){
            card = mcard;
        }
    }
}