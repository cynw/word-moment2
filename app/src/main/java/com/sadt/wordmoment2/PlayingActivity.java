package com.sadt.wordmoment2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sadt.wordmoment2.Deck.DeckActivity;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;


public class PlayingActivity extends Activity {
    public static ImageView image1, image2, image3, image4;
    public static ProgressBar progress1, progress2, progress3, progress4;
    public static List<Card> cards;
    public static List<Integer> answers;
    private static Context context;
    private int play;
    private Button answer1, answer2, answer3, answer4, skip, answer_btn;
    private LinearLayout answer_row, row1, row2, image_row;
    private TextView word1, word2, mainword, title, characters_hint;
    private EditText answer_edittext;
    private int deckId;
    private int size;
    private String forLan, localLan;
    private Deck deck;
    private int done = 0;
    private List<Integer> keys;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playing);
        context = this;
        Intent intent = getIntent();
        play = intent.getIntExtra("play", -1);
        deckId = intent.getIntExtra("deckId", -1);
        answer_row = (LinearLayout) findViewById(R.id.answer_row);
        row1 = (LinearLayout) findViewById(R.id.row1);
        row2 = (LinearLayout) findViewById(R.id.row2);
        image_row = (LinearLayout) findViewById(R.id.image_row);
        word1 = (TextView) findViewById(R.id.word1);
        word2 = (TextView) findViewById(R.id.word2);
        mainword = (TextView) findViewById(R.id.main_word);
        title = (TextView) findViewById(R.id.playing_title);
        answer_edittext = (EditText) findViewById(R.id.answer_edittext);
        answer1 = (Button) findViewById(R.id.answer1);
        answer2 = (Button) findViewById(R.id.answer2);
        answer3 = (Button) findViewById(R.id.answer3);
        answer4 = (Button) findViewById(R.id.answer4);
        answer_btn = (Button) findViewById(R.id.answer_btn);
        characters_hint = (TextView) findViewById(R.id.characters_hint);
        skip = (Button) findViewById(R.id.skip_btn);
        image1 = (ImageView) findViewById(R.id.image1);
        image2 = (ImageView) findViewById(R.id.image2);
        image3 = (ImageView) findViewById(R.id.image3);
        image4 = (ImageView) findViewById(R.id.image4);
        progress1 = (ProgressBar) findViewById(R.id.progress1);
        progress2 = (ProgressBar) findViewById(R.id.progress2);
        progress3 = (ProgressBar) findViewById(R.id.progress3);
        progress4 = (ProgressBar) findViewById(R.id.progress4);
        answers = new ArrayList<Integer>();
        deck = DeckActivity.deck;
        forLan = deck.getForeignLan();
        localLan = deck.getLocalLan();
        cards = DeckActivity.cards;
        size = cards.size();
        title.setText("0 / " + String.valueOf(size));
        Collections.shuffle(cards);
        game_prepare();
        setView();
        display();
    }

    public void setView() {
        switch (play) {
            case 0:
                word1.setText(cards.get(done).getForeign());
                if (keys.get(done) == 3)
                    word2.setText(cards.get(done).getLocal());
                else {
                    Random rand = new Random();
                    word2.setText(cards.get(rand.nextInt(size)).getLocal());
                }
                break;
            case 1:
                mainword.setText(cards.get(done).getForeign());
                List<Integer> random = new ArrayList<Integer>();
                for (int i = 0; i < size; i++) {
                    if (i == done)
                        continue;
                    random.add(i);
                }
                Collections.shuffle(random);
                final List<Integer> randoms = random;
                if (keys.get(done) == 1) {
                    answer1.setText(cards.get(done).getLocal());
                    answer2.setText(cards.get(randoms.get(0)).getLocal());
                    answer3.setText(cards.get(randoms.get(1)).getLocal());
                    answer4.setText(cards.get(randoms.get(2)).getLocal());
                } else if (keys.get(done) == 2) {
                    answer1.setText(cards.get(randoms.get(0)).getLocal());
                    answer2.setText(cards.get(done).getLocal());
                    answer3.setText(cards.get(randoms.get(1)).getLocal());
                    answer4.setText(cards.get(randoms.get(2)).getLocal());
                } else if (keys.get(done) == 3) {
                    answer1.setText(cards.get(randoms.get(0)).getLocal());
                    answer2.setText(cards.get(randoms.get(1)).getLocal());
                    answer3.setText(cards.get(done).getLocal());
                    answer4.setText(cards.get(randoms.get(2)).getLocal());
                } else {
                    answer1.setText(cards.get(randoms.get(0)).getLocal());
                    answer2.setText(cards.get(randoms.get(1)).getLocal());
                    answer3.setText(cards.get(randoms.get(2)).getLocal());
                    answer4.setText(cards.get(done).getLocal());
                }
                break;
            case 2:
                mainword.setText(cards.get(done).getLocal());
                characters_hint.setText(String.valueOf(cards.get(done).getForeign().length()) + " " + getResources().getString(R.string.characters));
                break;
            case 3:
                characters_hint.setText(String.valueOf(cards.get(done).getForeign().length()) + " " + getResources().getString(R.string.characters));
                image1.setVisibility(View.GONE);
                image2.setVisibility(View.GONE);
                image3.setVisibility(View.GONE);
                image4.setVisibility(View.GONE);
                progress1.setVisibility(View.VISIBLE);
                progress2.setVisibility(View.VISIBLE);
                progress3.setVisibility(View.VISIBLE);
                progress4.setVisibility(View.VISIBLE);
                new LoadImage(cards.get(done).getLocal()).execute();
                break;
        }
    }

    public void display() {
        switch (play) {
            case 0:
                image_row.setVisibility(View.GONE);
                answer_row.setVisibility(View.GONE);
                answer_btn.setVisibility(View.GONE);
                row1.setVisibility(View.GONE);
                mainword.setVisibility(View.GONE);
                answer3.setText(R.string.correct);
                answer4.setText(R.string.incorrect);
                break;
            case 1:
                image_row.setVisibility(View.GONE);
                word1.setVisibility(View.GONE);
                word2.setVisibility(View.GONE);
                answer_row.setVisibility(View.GONE);
                answer_btn.setVisibility(View.GONE);
                break;
            case 2:
                image_row.setVisibility(View.GONE);
                word1.setVisibility(View.GONE);
                word2.setVisibility(View.GONE);
                row1.setVisibility(View.GONE);
                row2.setVisibility(View.GONE);
                break;
            case 3:
                mainword.setVisibility(View.GONE);
                word1.setVisibility(View.GONE);
                word2.setVisibility(View.GONE);
                row1.setVisibility(View.GONE);
                row2.setVisibility(View.GONE);
                break;
            case 4:
                word1.setVisibility(View.GONE);
                word2.setVisibility(View.GONE);
                answer_row.setVisibility(View.GONE);
                answer_btn.setVisibility(View.GONE);
                break;
            case 5:
                word1.setVisibility(View.GONE);
                word2.setVisibility(View.GONE);
                answer_row.setVisibility(View.GONE);
                answer_btn.setVisibility(View.GONE);
                break;
        }
    }

    public void not_enough_card() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.not_enough_msg)
                .setTitle(R.string.not_enough_title)
                .setNeutralButton(R.string.not_enough_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void game_prepare() {
        switch (play) {
            case 0:
                if (size < 4)
                    not_enough_card();
                keys = new ArrayList<Integer>();
                for (int i = 0; i < (size / 2); i++)
                    keys.add(3);
                for (int i = (size / 2); i < size; i++)
                    keys.add(4);
                Collections.shuffle(keys);
                break;
            case 1:
                if (size < 4)
                    not_enough_card();
                keys = new ArrayList<Integer>();
                for (int i = 0; i < (size / 4); i++)
                    keys.add(1);
                for (int i = (size / 4); i < (size / 2); i++)
                    keys.add(2);
                for (int i = (size / 2); i < ((size * 3) / 4); i++)
                    keys.add(3);
                for (int i = ((size * 3) / 4); i < size; i++)
                    keys.add(4);
                Collections.shuffle(keys);
                break;
            case 2:
                answer_edittext.setHint(getResources().getString(R.string.answer_in) + " " + forLan);
                break;
            case 3:
                answer_edittext.setHint(getResources().getString(R.string.answer_in) + " " + forLan);
                break;
        }
    }

    public void next() {
        done++;
        if (done == size) {
            quit(null);
        } else {
            title.setText(String.valueOf(done) + " / " + String.valueOf(size));
            setView();
        }
    }

    public void skip(View view) {
        answers.add(0);
        if (play == 0 || play == 1 || play == 0 || play == 4 || play == 5) {
            if (keys.get(done).equals(1)) {
                answer1.setBackgroundResource(R.color.playing_incorrect);
            } else if (keys.get(done).equals(2)) {
                answer2.setBackgroundResource(R.color.playing_incorrect);
            } else if (keys.get(done).equals(3)) {
                answer3.setBackgroundResource(R.color.playing_incorrect);
            } else {
                answer4.setBackgroundResource(R.color.playing_incorrect);
            }
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    answer1.setBackgroundResource(R.color.dark_button_bg);
                    answer2.setBackgroundResource(R.color.dark_button_bg);
                    answer3.setBackgroundResource(R.color.dark_button_bg);
                    answer4.setBackgroundResource(R.color.dark_button_bg);
                    next();
                }
            }, 500);
        } else {
            answer_btn.setBackgroundResource(R.color.playing_incorrect);
            answer_edittext.setText(cards.get(done).getForeign());
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    answer_btn.setBackgroundResource(R.color.dark_button_bg);
                    answer_edittext.setText("");
                    next();
                }
            }, 500);
        }
    }

    public void choice_answer(View view) {
        int answer = 0;
        if (view.getId() == R.id.answer1)
            answer = 1;
        else if (view.getId() == R.id.answer2)
            answer = 2;
        else if (view.getId() == R.id.answer3)
            answer = 3;
        else if (view.getId() == R.id.answer4)
            answer = 4;

        if (answer == keys.get(done)) {
            Log.d("Answer", "Correct");
            answers.add(1);
            view.setBackgroundResource(R.color.playing_correct);
        } else {
            Log.d("Answer", String.valueOf(keys.get(done)));
            answers.add(-1);
            if (keys.get(done).equals(1)) {
                answer1.setBackgroundResource(R.color.playing_incorrect);
            } else if (keys.get(done).equals(2)) {
                answer2.setBackgroundResource(R.color.playing_incorrect);
            } else if (keys.get(done).equals(3)) {
                answer3.setBackgroundResource(R.color.playing_incorrect);
            } else {
                answer4.setBackgroundResource(R.color.playing_incorrect);
            }
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                answer1.setBackgroundResource(R.color.darkbutton_bg);
                answer2.setBackgroundResource(R.color.darkbutton_bg);
                answer3.setBackgroundResource(R.color.darkbutton_bg);
                answer4.setBackgroundResource(R.color.darkbutton_bg);
                answer_btn.setBackgroundResource(R.color.darkbutton_bg);
                next();
            }
        }, 500);
    }

    public void type_answer(View view) {
        String answer = answer_edittext.getText().toString();
        if (answer.equals(cards.get(done).getForeign())) {
            answers.add(1);
            view.setBackgroundResource(R.color.playing_correct);
        } else {
            answers.add(-1);
            view.setBackgroundResource(R.color.playing_incorrect);
            answer_edittext.setText(cards.get(done).getForeign());
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                answer_btn.setBackgroundResource(R.color.dark_button_bg);
                answer_edittext.setText("");
                next();
            }
        }, 500);
    }

    public void quit(View view) {
        if (done == 0)
            finish();
        else {
            Intent intent = new Intent(this, SummaryActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
