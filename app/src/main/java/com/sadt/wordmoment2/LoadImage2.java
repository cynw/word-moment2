package com.sadt.wordmoment2;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.InputStream;

/**
 * Created by Chayanin on 8/3/2558.
 */
public class LoadImage2 extends AsyncTask<Void, Void, Bitmap> {
    private String url;
    private ImageView imageView;
    private ProgressBar progressBar;

    public LoadImage2(String mUrl, ImageView mImageView, ProgressBar mProgressBar) {
        url = mUrl;
        imageView = mImageView;
        progressBar = mProgressBar;
    }

    @Override
    protected Bitmap doInBackground(Void... params) {
        Bitmap mBitmap = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mBitmap = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mBitmap;
    }

    protected void onPostExecute(Bitmap mBitmap) {
        progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        imageView.setImageBitmap(mBitmap);
    }
}
