package com.sadt.wordmoment2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sadt.wordmoment2.Decks.ManageDecksActivity;
import com.sadt.wordmoment2.Object.Deck;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<String> expandableListTitle;
    private HashMap<String, List<Deck>> expandableListDetail;
    private Boolean edit;
    private HashMap<Deck, Boolean> isSelected;
    private ViewHolder holder = null;

    public ExpandableListAdapter(Context context, List<String> expandableListTitle,
                                 HashMap<String, List<Deck>> expandableListDetail, Boolean edit) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
        this.edit = edit;
        this.isSelected = new HashMap();
        setAllChecked(false);
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final Deck deck = (Deck) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.decks_item, null);

            holder = new ViewHolder();

            holder.deckname = (TextView) convertView.findViewById(R.id.deckname);
            holder.deckname2 = (TextView) convertView.findViewById(R.id.deckname2);
            holder.edit_btn = (ImageView) convertView.findViewById(R.id.edit_btn);
            convertView.setTag(R.string.tag1 ,holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag(R.string.tag1);
        }
        final String expandedListText = deck.getDeckName();
        final String expandedListText2 = deck.getDeckName2();

        if (expandedListText2 == null){
            holder.deckname2.setText(expandedListText);
        }
        else{
            holder.deckname.setText(expandedListText);
            holder.deckname2.setText(expandedListText2);
        }

        if (edit) holder.edit_btn.setVisibility(View.VISIBLE);

        if (!isSelected.get(deck))
            holder.edit_btn.setImageResource(R.drawable.select_u);
        else
            holder.edit_btn.setImageResource(R.drawable.select_a);

        convertView.setTag(R.string.tag2,deck.getDeckId());
        if (edit)
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setChecked(deck);
                    notifyDataSetChanged();
                    int count = getChecked().size();
                    if (count>0) ManageDecksActivity.count_tv.setText("("+count+")");
                    else ManageDecksActivity.count_tv.setText("");
                }
            });
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String listTitle = (String) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.decks_group, null);
        }
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.listTitle);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(display_lan(listTitle));
        
        
        return convertView;
    }

    public String display_lan(String eng){
        String[] languages = MainActivity.context.getResources().getStringArray(R.array.languages);
        String[] display_lan = MainActivity.context.getResources().getStringArray(R.array.display_lan);

        for (int i=0; i<languages.length; i++){
            if (eng.equals(languages[i]))
                return display_lan[i];
        }
        return "";
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    public void setAllChecked(boolean ischeked) {
        isSelected.clear();
        for (Map.Entry<String, List<Deck>> entry : expandableListDetail.entrySet()) {
            for (Deck deck : entry.getValue()){
                isSelected.put(deck, ischeked);
            }
        }
    }

    public ArrayList<Deck> getChecked(){
        ArrayList<Deck> mArrayList = new ArrayList<>();
        for (Map.Entry<Deck, Boolean> entry : isSelected.entrySet()) {
            if (entry.getValue())
                mArrayList.add(entry.getKey());
        }
        return mArrayList;
    }

    public void setChecked(Deck mdeck) {
        if (isSelected.get(mdeck))
            isSelected.put(mdeck, false);
        else
            isSelected.put(mdeck, true);
    }

    public class ViewHolder {
        private TextView deckname, deckname2;
        private ImageView edit_btn;
    }

    public void remove(Deck deck){
        isSelected.remove(deck);
        for (Map.Entry<String, List<Deck>> entry : expandableListDetail.entrySet()) {
            List<Deck> decks = entry.getValue();
            String lan = entry.getKey();
            if (decks.contains(deck)){
                expandableListDetail.remove(lan);
                decks.remove(deck);
                if (decks.size() != 0)
                    expandableListDetail.put(lan, decks);
                else
                    expandableListTitle.remove(lan);
                return;
            }
        }
        notifyDataSetChanged();
    }
}