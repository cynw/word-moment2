package com.sadt.wordmoment2;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Activity which displays a registration screen to the user.
 */
public class WelcomeActivity extends Activity implements GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener{
	CallbackManager callbackManager;

	/* Request code used to invoke sign in user interactions. */
	private static final int RC_SIGN_IN = 0;

	/* Client used to interact with Google APIs. */
	private GoogleApiClient mGoogleApiClient;
    /* Is there a ConnectionResult resolution in progress? */
    private boolean mIsResolving = false;

    /* Should we automatically resolve ConnectionResults when possible? */
    private boolean mShouldResolve = false;

	private static Context context;
	private String name, email, id;


	@Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_welcome);

		context = this;

    // Log in button click handler
    Button loginButton = (Button) findViewById(R.id.login_button);
    loginButton.setOnClickListener(new OnClickListener() {
		public void onClick(View v) {
			// Starts an intent of the log in activity
			startActivity(new Intent(WelcomeActivity.this, LoginActivity.class));
		}
	});

    // Sign up button click handler
    Button signupButton = (Button) findViewById(R.id.signup_button);
    signupButton.setOnClickListener(new OnClickListener() {
      public void onClick(View v) {
        // Starts an intent for the sign up activity
        startActivity(new Intent(WelcomeActivity.this, SignUpActivity.class));
      }
    });

	// Build GoogleApiClient with access to basic profile
	mGoogleApiClient = new GoogleApiClient.Builder(this)
			.addConnectionCallbacks(this)
			.addOnConnectionFailedListener(this)
			.addApi(Plus.API)
			.addScope(new Scope(Scopes.PROFILE))
			.build();

        findViewById(R.id.google_sign_in_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mShouldResolve = true;
                mGoogleApiClient.connect();
            }
        });

    
    FacebookSdk.sdkInitialize(getApplicationContext());
    callbackManager = CallbackManager.Factory.create();
    LoginButton fbButton = (LoginButton) findViewById(R.id.facebook_login_button);
    fbButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

		@Override
		public void onSuccess(LoginResult result) {
			GraphRequest request = GraphRequest.newMeRequest( result.getAccessToken(),new GraphRequest.GraphJSONObjectCallback() {
				@Override
				public void onCompleted(JSONObject object, GraphResponse response) {
					Log.d("What:", object.toString());
					try {
						id =object.getString("id");
						name=object.getString("name");
					} catch (JSONException e) {
						Toast.makeText(WelcomeActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
					}
					ParseQuery<ParseUser> query = ParseUser.getQuery();
					query.whereEqualTo("username", id);
					try {
						List<ParseUser> users = query.find();
						if(users.size() == 0){
							Intent intent = new Intent(WelcomeActivity.context, TermsActivity.class);
							intent.putExtra("signup", 1);
							startActivityForResult(intent, 1);

							/*ParseUser user = new ParseUser();
						    user.setUsername(id);
						    user.setPassword("฿");
						    user.put("name", name);
						    user.signUpInBackground(new SignUpCallback() {
						        @Override
						        public void done(ParseException e) {
						          if (e != null) {
						            // Show the error message
						            Toast.makeText(WelcomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
						          } else {
						            // Start an intent for the dispatch activity
						            Intent intent = new Intent(WelcomeActivity.this, DispatchActivity.class);
						            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
						            startActivity(intent);
						          }
						        }
						      });*/
						}
						else{
							ParseUser.logInInBackground(id, "฿", new LogInCallback() {
								@Override
								public void done(ParseUser user, ParseException e) {
									if (e != null) {
										// Show the error message
										Toast.makeText(WelcomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
									} else {
										// Start an intent for the dispatch activity
										Intent intent = new Intent(WelcomeActivity.this, DispatchActivity.class);
										intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
										startActivity(intent);
									}
								}
							});
						}
					} catch (ParseException e) {
						Toast.makeText(WelcomeActivity.this, "get user error", Toast.LENGTH_SHORT).show();
					}
				}
			});
			Bundle parameters = new Bundle();
			parameters.putString("fields", "id,name");
			request.setParameters(parameters);
			request.executeAsync();
			Toast.makeText(WelcomeActivity.this, result.getAccessToken().getUserId(), Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onCancel() {
			Toast.makeText(WelcomeActivity.this, R.string.facebook_login_cancel, Toast.LENGTH_SHORT).show();
		}

		@Override
		public void onError(FacebookException error) {
			Toast.makeText(WelcomeActivity.this, R.string.facebook_login_error, Toast.LENGTH_SHORT).show();
		}
    	
    });
  }
  
  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      super.onActivityResult(requestCode, resultCode, data);

      if (requestCode == RC_SIGN_IN) {
          // If the error resolution was not successful we should not resolve further.
          if (resultCode != RESULT_OK) {
              mShouldResolve = false;
          }
          mIsResolving = false;
          mGoogleApiClient.connect();
      }
        else
          callbackManager.onActivityResult(requestCode, resultCode, data);

	  if (requestCode == 1 && resultCode == RESULT_OK) {
		  // Set up a progress dialog
		  final ProgressDialog dialog = new ProgressDialog(WelcomeActivity.this);
		  dialog.setMessage(getString(R.string.progress_signup));
		  dialog.show();

		  ParseUser user = new ParseUser();
		  user.setUsername(id);
		  if (email!=null) user.put("email", email);
		  user.setPassword("฿");
		  user.put("name", name);
		  user.put("alarm", false);
		  user.put("walnut", 0);
		  user.signUpInBackground(new SignUpCallback() {
			  @Override
			  public void done(ParseException e) {
				  if (e != null) {
					  // Show the error message
					  Toast.makeText(WelcomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
				  } else {
					  // Start an intent for the dispatch activity
					  Intent intent = new Intent(WelcomeActivity.this, DispatchActivity.class);
					  intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
					  startActivity(intent);
				  }
			  }
		  });
	  }
  }

	@Override
	protected void onStop() {
		super.onStop();
		mGoogleApiClient.disconnect();
	}

    @Override
    public void onConnected(Bundle bundle) {
        // onConnected indicates that an account was selected on the device, that the selected
        // account has granted any requested permissions to our app and that we were able to
        // establish a service connection to Google Play services.
        mShouldResolve = false;
        if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
            name = currentPerson.getDisplayName();
            email = Plus.AccountApi.getAccountName(mGoogleApiClient);
            id = currentPerson.getId();
            Log.d("name: ", name);
			Log.d("email: ", email);
			Log.d("id: ", id);
			ParseQuery<ParseUser> query = ParseUser.getQuery();
			query.whereEqualTo("username", id);
			try {
				List<ParseUser> users = query.find();
				if(users.size() == 0){
					Intent intent = new Intent(WelcomeActivity.context, TermsActivity.class);
					intent.putExtra("signup", 1);
					startActivityForResult(intent, 1);

					/*ParseUser user = new ParseUser();
					user.setUsername(id);
					user.put("email", email);
					user.setPassword("฿");
					user.put("name", name);
					user.signUpInBackground(new SignUpCallback() {
						@Override
						public void done(ParseException e) {
							if (e != null) {
								// Show the error message
								Toast.makeText(WelcomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
							} else {
								// Start an intent for the dispatch activity
								Intent intent = new Intent(WelcomeActivity.this, DispatchActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
							}
						}
					});*/
				}
				else{
					ParseUser.logInInBackground(id, "฿", new LogInCallback() {
						@Override
						public void done(ParseUser user, ParseException e) {
							if (e != null) {
								// Show the error message
								Toast.makeText(WelcomeActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
							} else {
								// Start an intent for the dispatch activity
								Intent intent = new Intent(WelcomeActivity.this, DispatchActivity.class);
								intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);
							}
						}
					});
				}
			} catch (ParseException e) {
				Toast.makeText(WelcomeActivity.this, "get user error", Toast.LENGTH_SHORT).show();
			}
        }


    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        // Could not connect to Google Play Services.  The user needs to select an account,
        // grant permissions or resolve an error in order to sign in. Refer to the javadoc for
        // ConnectionResult to see possible error codes.

        if (!mIsResolving && mShouldResolve) {
            if (connectionResult.hasResolution()) {
                try {
                    connectionResult.startResolutionForResult(this, RC_SIGN_IN);
                    mIsResolving = true;
                } catch (IntentSender.SendIntentException e) {
                    mIsResolving = false;
                    mGoogleApiClient.connect();
                }
            } else {
                // Could not resolve the connection result, show the user an
                // error dialog.
                Log.d("Error: ", "Could not resolve the connection result");
            }
        } else {
            // Show the signed-out UI
            Log.d("Error: ", "Signing out");
        }

    }
}
