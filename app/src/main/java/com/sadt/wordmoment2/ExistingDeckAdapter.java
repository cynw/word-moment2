package com.sadt.wordmoment2;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.parse.ParseObject;

/**
 * Created by Chayanin on 18/3/2558.
 */
public class ExistingDeckAdapter extends ArrayAdapter<ParseObject> {
    private List<ParseObject> decks;
    private Context context;
    private TextView deckname_tv, deckname2_tv, deckprice_tv;
    private Boolean purchased;

    public ExistingDeckAdapter(Context context, int textViewResourceId, List<ParseObject> mDecks, Boolean mpurchased){
        super(context,textViewResourceId, mDecks);
        this.context = context;
        this.decks = mDecks;
        this.purchased = mpurchased;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = vi.inflate(R.layout.existing_deck_item, parent, false);

        deckname_tv = (TextView) convertView.findViewById(R.id.deckname_tv);
        deckname2_tv = (TextView) convertView.findViewById(R.id.deckname2_tv);
        deckprice_tv = (TextView) convertView.findViewById(R.id.deckprice_tv);

        String deckName = decks.get(position).getString("deckName");
        String deckName2 = decks.get(position).getString("deckName2");
        Boolean free = decks.get(position).getBoolean("free");

        if(deckName!=null) deckname_tv.setText(deckName);
        if(deckName2!=null) deckname2_tv.setText(deckName2);

        if (free || purchased)
            deckprice_tv.setText(R.string.existingdeck_download);
        else
            deckprice_tv.setText(R.string.existingdeck_extra);

        convertView.setTag(free);
        return convertView;
    }
}
