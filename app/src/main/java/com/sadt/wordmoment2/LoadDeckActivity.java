package com.sadt.wordmoment2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;

import java.util.ArrayList;
import java.util.List;

public class LoadDeckActivity extends AppCompatActivity {
    private TextView deckname1_tv, deckname2_tv, locallan_tv, forlan_tv;
    private ProgressBar loaddeck_pb, seedeck_pb;
    private EditText deckid_et;
    private LinearLayout deckinfo_lo;
    private Button load_btn;
    private Deck deck;
    private String deckId;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_deck);


        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        deckid_et = (EditText)findViewById(R.id.deckid_et);
        deckname1_tv = (TextView) findViewById(R.id.deckname1_tv);
        deckname2_tv = (TextView) findViewById(R.id.deckname2_tv);
        locallan_tv = (TextView) findViewById(R.id.locallan_tv);
        forlan_tv = (TextView)findViewById(R.id.forlan_tv);
        loaddeck_pb = (ProgressBar)findViewById(R.id.loaddeck_pb);
        seedeck_pb =(ProgressBar)findViewById(R.id.seedeck_pb);
        deckinfo_lo = (LinearLayout)findViewById(R.id.deckinfo_lo);
        load_btn = (Button)findViewById(R.id.load_btn);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        Intent intent = getIntent();
        deckId = intent.getStringExtra("deckId");
        if (!deckId.equals("none")){
            deckid_et.setText(deckId);
            see_deck(getCurrentFocus());
        }
    }

    public void see_deck(View v){
        seedeck_pb.setVisibility(View.VISIBLE);
        int tid = Integer.valueOf(deckid_et.getText().toString().trim());
        ParseQuery<Deck> deckQuery = Deck.getQuery();
        deckQuery.whereEqualTo("deckId", tid);
        deckQuery.getFirstInBackground(new GetCallback<Deck>() {
            @Override
            public void done(Deck mdeck, ParseException e) {
                if (e==null){
                    deck = mdeck;
                    deckname1_tv.setText(deck.getDeckName());
                    deckname2_tv.setText(deck.getDeckName2());
                    locallan_tv.setText(getString(R.string.loaddeck_from)+" "+dis_lan(deck.getLocalLan()));
                    forlan_tv.setText(getString(R.string.loaddeck_learning)+" "+dis_lan(deck.getForeignLan()));
                    load_btn.setVisibility(View.VISIBLE);
                    load_btn.setClickable(true);
                    load_btn.setText(R.string.loaddeck_load);
                }
                else{
                    deckname1_tv.setText(R.string.loaddeck_notfound_title);
                    deckname2_tv.setText(R.string.loaddeck_notfound_text);
                }
                seedeck_pb.setVisibility(View.INVISIBLE);
                deckinfo_lo.setVisibility(View.VISIBLE);
            }
        });
    }

    public void load_deck(View v){
        load_btn.setVisibility(View.INVISIBLE);
        loaddeck_pb.setVisibility(View.VISIBLE);
        final Deck ndeck = new Deck();
        ndeck.clone(deck, ParseUser.getCurrentUser());
        final List<Card> ncards = new ArrayList<>();
        ParseQuery<Card> cardQuery = Card.getQuery();
        cardQuery.whereEqualTo("deck", deck);
        cardQuery.findInBackground(new FindCallback<Card>() {
            @Override
            public void done(List<Card> cards, ParseException e) {
                loaddeck_pb.setMax(cards.size());
                int count = 0;
                for (Card card : cards) {
                    Card ncard = new Card();
                    ncard.clone(card, ndeck);
                    ncards.add(ncard);
                    loaddeck_pb.setProgress(count++);
                }
                Card.saveAllInBackground(ncards, new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        loaddeck_pb.setVisibility(View.INVISIBLE);
                        load_btn.setVisibility(View.VISIBLE);
                        load_btn.setClickable(false);
                        load_btn.setText(R.string.loaddeck_done);
                    }
                });
            }
        });
    }

    public String dis_lan(String eng){
        String[] languegs = getResources().getStringArray(R.array.languages);
        String[] display_lan = getResources().getStringArray(R.array.display_lan);

        for (int i=0; i<languegs.length; i++){
            if (eng.equals(languegs[i]))
                return display_lan[i];
        }
        return "";
    }
}
