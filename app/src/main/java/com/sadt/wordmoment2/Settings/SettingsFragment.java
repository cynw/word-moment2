package com.sadt.wordmoment2.Settings;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sadt.wordmoment2.MainActivity;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;
import com.sadt.wordmoment2.TermsActivity;

import java.util.Calendar;
import java.util.List;


public class SettingsFragment extends Fragment {
    private Switch alarm_sw;
    private static TextView time_tv;
    private static AlarmReceiver alarm;
    private FragmentManager fragmentManager;
    private ParseUser me;
    private ImageButton sync_btn;
    private ProgressBar sync_pb;
    private RelativeLayout feedbacks_lo, terms_lo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        alarm_sw = (Switch) rootView.findViewById(R.id.alarm_sw);
        time_tv = (TextView) rootView.findViewById(R.id.time_tv);
        sync_btn = (ImageButton) rootView.findViewById(R.id.sync_btn);
        sync_pb = (ProgressBar) rootView.findViewById(R.id.sync_pb);
        terms_lo = (RelativeLayout) rootView.findViewById(R.id.terms_lo);
        feedbacks_lo = (RelativeLayout) rootView.findViewById(R.id.feedbacks_lo);

        fragmentManager = getFragmentManager();

        alarm = new AlarmReceiver();

        me = ParseUser.getCurrentUser();

        if(me.getBoolean("alarm")) {
            alarm_sw.setChecked(true);
            time_tv.setText(String.valueOf(me.getInt("hour"))+":"+String.valueOf(me.getInt("minute")));
        }

        alarm_sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    TimePickerFragment newFragment = new TimePickerFragment();
                    newFragment.show(getActivity().getSupportFragmentManager(), "timepicker");
                }
                else {
                    me.put("alarm",false);
                    me.saveEventually();
                    alarm.cancelAlarm(MainActivity.context);
                    time_tv.setText(R.string.alarm_disabled);
                }
            }
        });

        sync_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sync_btn.setVisibility(View.INVISIBLE);
                sync_pb.setVisibility(View.VISIBLE);
                ParseQuery<Deck> deckQuery = Deck.getQuery();
                deckQuery.whereEqualTo("user", me);
                deckQuery.findInBackground(new FindCallback<Deck>() {
                    @Override
                    public void done(List<Deck> decks, ParseException e) {
                        if (e==null) {
                            Deck.pinAllInBackground(decks);
                        }
                    }
                });
                ParseQuery<Card> query = Card.getQuery();
                query.whereMatchesQuery("deck", deckQuery);
                query.findInBackground(new FindCallback<Card>() {
                    @Override
                    public void done(List<Card> cards, ParseException e) {
                        if(e==null) {
                            Card.pinAllInBackground(cards, new SaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    sync_pb.setVisibility(View.INVISIBLE);
                                    sync_btn.setVisibility(View.VISIBLE);
                                    sync_btn.setImageResource(R.drawable.ic_done_black_24dp);
                                    sync_btn.setEnabled(false);
                                }
                            });
                        }
                        else{
                            sync_pb.setVisibility(View.INVISIBLE);
                            sync_btn.setVisibility(View.VISIBLE);
                            sync_btn.setImageResource(R.drawable.ic_done_black_24dp);
                            sync_btn.setEnabled(false);
                        }
                    }
                });
            }
        });

        feedbacks_lo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.context, FeedbackActivity.class);
                startActivity(intent);
            }
        });

        terms_lo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.context, TermsActivity.class);
                startActivity(intent);
            }
        });

        return rootView;
    }

    public static class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {
        private ParseUser me;

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            me = ParseUser.getCurrentUser();

            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hour, int minute) {
            alarm.setAlarm(MainActivity.context, hour, minute);
            me.put("alarm", true);
            me.put("hour", hour);
            me.put("minute", minute);
            me.saveEventually();
            time_tv.setText(String.valueOf(hour)+":"+minute);
        }
    }
}
