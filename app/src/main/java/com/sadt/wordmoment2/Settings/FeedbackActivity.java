package com.sadt.wordmoment2.Settings;

import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.ParseObject;
import com.parse.ParseUser;
import com.sadt.wordmoment2.R;


public class FeedbackActivity extends AppCompatActivity {
    private EditText feedback_edittext;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(R.string.feedback_actionbar);
        
        feedback_edittext = (EditText) findViewById(R.id.feedback_edittext);
    }

    public void back(View view){
        finish();
    }

    public void send(View view){
        String feedbacks = feedback_edittext.getText().toString().trim();
        if (feedbacks.length() == 0)
            Toast.makeText(this, R.string.comments_no, Toast.LENGTH_SHORT).show();
        else{
            ParseObject feedback = new ParseObject("Feedback");
            feedback.put("text", feedbacks);
            feedback.put("user", ParseUser.getCurrentUser());
            feedback.saveInBackground();
            Toast.makeText(this, R.string.comments_thanks, Toast.LENGTH_SHORT).show();
            finish();
        }
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        // Respond to the action bar's Up/Home button
        case android.R.id.home:
            finish();
            //NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
