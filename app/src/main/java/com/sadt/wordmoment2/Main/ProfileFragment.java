package com.sadt.wordmoment2.Main;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sadt.wordmoment2.DispatchActivity;
import com.sadt.wordmoment2.LanguageActivity;
import com.sadt.wordmoment2.MainActivity;
import com.sadt.wordmoment2.R;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment {
    private Button more_decks_btn, logout_btn, changepwd_btn;
    private TextView welcome_tv, score_tv;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        more_decks_btn = (Button) rootView.findViewById(R.id.more_decks_btn);
        logout_btn = (Button) rootView.findViewById(R.id.logout_btn);
        welcome_tv = (TextView) rootView.findViewById(R.id.welcome_tv);
        score_tv = (TextView) rootView.findViewById(R.id.score_tv);
        changepwd_btn = (Button) rootView.findViewById(R.id.changepwd_btn);

        String name = ParseUser.getCurrentUser().getString("name");
        int walnut = ParseUser.getCurrentUser().getInt("walnut");

        welcome_tv.setText(getString(R.string.profile_welcome) + " " + name);
        score_tv.setText(getString(R.string.profile_score)+ " "+String.valueOf(walnut));

        changepwd_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.context);
                LayoutInflater inflater = getActivity().getLayoutInflater();
                View dialogv = inflater.inflate(R.layout.dialog_password, null);
                final EditText current_pwd_et = (EditText) dialogv.findViewById(R.id.current_pwd_et);
                final EditText new_pwd_et = (EditText) dialogv.findViewById(R.id.new_pwd_et);
                final EditText new_pwd_again_et = (EditText) dialogv.findViewById(R.id.new_pwd_again_et);
                new_pwd_et.setVisibility(View.GONE);
                new_pwd_again_et.setVisibility(View.GONE);
                builder.setView(dialogv);
                builder.setPositiveButton(R.string.profile_change_pwd_next, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String current = current_pwd_et.getText().toString();

                        ParseUser.logInInBackground(ParseUser.getCurrentUser().getUsername(), current, new LogInCallback() {
                            public void done(ParseUser user, ParseException e) {
                                if (user != null) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.context);
                                    LayoutInflater inflater = getActivity().getLayoutInflater();
                                    View dialogv = inflater.inflate(R.layout.dialog_password, null);
                                    final EditText current_pwd_et = (EditText) dialogv.findViewById(R.id.current_pwd_et);
                                    final EditText new_pwd_et = (EditText) dialogv.findViewById(R.id.new_pwd_et);
                                    final EditText new_pwd_again_et = (EditText) dialogv.findViewById(R.id.new_pwd_again_et);
                                    current_pwd_et.setVisibility(View.GONE);
                                    builder.setView(dialogv);
                                    builder.setPositiveButton(R.string.profile_change_pwd_change, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            String new_pwd = new_pwd_et.getText().toString();
                                            String new_pwd_again = new_pwd_et.getText().toString();

                                            if (!new_pwd.equals(new_pwd_again)) return;

                                            ParseUser parseUser = ParseUser.getCurrentUser();
                                            parseUser.setPassword(new_pwd);
                                            parseUser.saveInBackground(new SaveCallback() {
                                                @Override
                                                public void done(ParseException e) {
                                                    if (null == e) {
                                                        Toast.makeText(MainActivity.context, R.string.profile_change_pwd_success, Toast.LENGTH_LONG).show();
                                                    }
                                                }
                                            });
                                        }
                                    });
                                    builder.setNegativeButton(R.string.profile_change_pwd_cancel, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    });
                                    builder.create().show();
                                } else {
                                    Toast.makeText(MainActivity.context, R.string.profile_pwd_incorrect, Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                    }
                });
                builder.setNegativeButton(R.string.profile_change_pwd_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create().show();
            }
        });

        more_decks_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.context, LanguageActivity.class);
                intent.putExtra("buy", true);
                startActivity(intent);
            }
        });

        logout_btn.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                ParseUser.logOut();
                Intent intent = new Intent(MainActivity.context, DispatchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });

        return rootView;
    }
}
