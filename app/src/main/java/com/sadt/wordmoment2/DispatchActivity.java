package com.sadt.wordmoment2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DispatchActivity extends AppCompatActivity {
  private SharedPreferences prefs = null;
    private static Context context;
    private int current;

  public DispatchActivity() {
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
      context = this;
    prefs = getSharedPreferences("com.sadt.wordmoment2", MODE_PRIVATE);
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (prefs.getBoolean("firstrun", true)) {
        current = 0;
        setContentView(R.layout.activity_dispatch);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FirstFragment fragment = new FirstFragment();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();

        prefs.edit().putBoolean("firstrun", false).commit();
    }else{
        if (ParseUser.getCurrentUser() != null) {
            Intent intent = getIntent();
            if (Intent.ACTION_VIEW.equals(intent.getAction())) {
                Uri uri = intent.getData();
                String deckId = uri.getQueryParameter("deckId"); // "str" is set
                if(deckId==null)
                    startActivity(new Intent(this, MainActivity.class));
                else {
                    Intent i = new Intent(this, LoadDeckActivity.class);
                    i.putExtra("deckId", deckId);
                    startActivity(i);
                }
            }
            else
                startActivity(new Intent(this, MainActivity.class));
        } else {
            startActivity(new Intent(this, WelcomeActivity.class));
        }
    }
  }

    public void skip(View v){
        startActivity(new Intent(this, WelcomeActivity.class));
    }

    public void next(View v){
        current++;
        if (current == 1){
            SecondFragment fragment = new SecondFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide1, R.anim.slide2);
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
        else if (current ==2){
            ThirdFragment fragment = new ThirdFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide1, R.anim.slide2);
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
        }
        else{
            startActivity(new Intent(this, WelcomeActivity.class));
        }
    }

    public static class FirstFragment extends Fragment {
        private View rootView;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_first, container, false);

            return rootView;
        }
    }

    public static class SecondFragment extends Fragment {
        private View rootView;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_second, container, false);

            return rootView;
        }
    }

    public static class ThirdFragment extends Fragment {
        private View rootView;
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.fragment_third, container, false);

            return rootView;
        }
    }

}
