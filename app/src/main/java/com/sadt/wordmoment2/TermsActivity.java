package com.sadt.wordmoment2;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Toast;

public class TermsActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private LinearLayout terms_agree_lo;
    private CheckBox agree_cb;
    private Button next_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        terms_agree_lo = (LinearLayout) findViewById(R.id.terms_agree_lo);
        agree_cb = (CheckBox) findViewById(R.id.agree_cb);
        next_btn = (Button) findViewById(R.id.next_btn);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.terms_title);

        Intent i = getIntent();
        if (i.getIntExtra("signup", 0) == 1){
            terms_agree_lo.setVisibility(View.VISIBLE);
        }


        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (agree_cb.isChecked()) {
                    Intent intent = new Intent();
                    setResult(RESULT_OK, intent);
                    finish();
                }
                else
                    Toast.makeText(TermsActivity.this, R.string.first_must_agree, Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
