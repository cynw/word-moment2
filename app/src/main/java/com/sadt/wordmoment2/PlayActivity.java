package com.sadt.wordmoment2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;


public class PlayActivity extends AppCompatActivity {
    private static Context context;
    private ListView plays_listview;
    private ArrayAdapter<String> adapter;
    private String[] plays_list;
    private int deckId;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        plays_listview = (ListView) findViewById(R.id.plays);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        context = this;
        Intent intent = getIntent();
        deckId = intent.getIntExtra("deckId", -1);
        plays_list = getResources().getStringArray(R.array.plays);
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, plays_list);
        plays_listview.setAdapter(adapter);
        plays_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(PlayActivity.context, PlayingActivity.class);
                intent.putExtra("deckId", deckId);
                intent.putExtra("play", position);
                if (position == 3 && !isOnline())
                    Toast.makeText(PlayActivity.context, R.string.play_internet, Toast.LENGTH_LONG).show();
                else {
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
