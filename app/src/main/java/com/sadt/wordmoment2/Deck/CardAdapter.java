package com.sadt.wordmoment2.Deck;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.R;

/**
 * Created by Chayanin on 2015-01-25.
 */
public class CardAdapter extends ArrayAdapter<Card> {

    private Context context;
    private String foreign, local;
    private List<Card> cards;
    private List<Integer> answers;
    private Boolean edit;
    private Boolean[] isChecked;
    private ViewHolder holder = null;

    public CardAdapter(Context context, int textViewResourceId, List<Card> mCards, List<Integer> mAnswers, Boolean edit) {
        super(context, textViewResourceId, mCards);
        this.context = context;
        this.cards = mCards;
        this.answers = mAnswers;
        this.edit = edit;
        this.isChecked = new Boolean[mCards.size()];
        setAllChecked(false);
    }

    public void setAllChecked(boolean ischeked) {
        int tempSize = isChecked.length;
        for(int a=0 ; a<tempSize ; a++){
            isChecked[a] = ischeked;
        }
    }

    public ArrayList<Card> getChecked(){
        int tempSize = isChecked.length;
        ArrayList<Card> mArrayList = new ArrayList<Card>();
        for(int b=0 ; b<tempSize ; b++){
            if(isChecked[b]){
                mArrayList.add(cards.get(b));
            }
        }
        return mArrayList;
    }

    public void setChecked(int position) {
        isChecked[position] = !isChecked[position];
    }

    public class ViewHolder {
        private TextView foreign_word, local_word;
        private ImageView status_picture, select_iv;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.answer_item, null);

            holder = new ViewHolder();

            holder.select_iv = (ImageView) convertView.findViewById(R.id.select_iv);
            holder.foreign_word = (TextView) convertView.findViewById(R.id.foreign_word);
            holder.local_word = (TextView) convertView.findViewById(R.id.local_word);
            holder.status_picture = (ImageView) convertView.findViewById(R.id.status_picture);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        foreign = cards.get(position).getForeign();
        local = cards.get(position).getLocal();

        if (edit)
            holder.select_iv.setVisibility(View.VISIBLE);

        holder.foreign_word.setText(foreign);
        holder.local_word.setText(local);
        if (answers != null && position < answers.size()) {
            switch (answers.get(position)) {
                case -1:
                    holder.status_picture.setImageResource(R.drawable.wrong);
                    break;
                case 0:
                    holder.status_picture.setImageResource(R.drawable.skip);
                    break;
                case 1:
                    holder.status_picture.setImageResource(R.drawable.correct);
                    break;
            }
        }

        if (isChecked[position])
            holder.select_iv.setImageResource(R.drawable.select_a);
        else
            holder.select_iv.setImageResource(R.drawable.select_u);

        return convertView;
    }
}
