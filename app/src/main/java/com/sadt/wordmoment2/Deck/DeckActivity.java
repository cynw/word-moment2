package com.sadt.wordmoment2.Deck;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.sadt.wordmoment2.Flashcard.FlashcardActivity;
import com.sadt.wordmoment2.ListenActivity;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.PlayActivity;
import com.sadt.wordmoment2.R;

import java.util.Date;
import java.util.List;

public class DeckActivity extends AppCompatActivity {
	private Toolbar toolbar;
    public static Deck deck;
    public static List<Card> cards;
    private static Context context;
    private int deckId;
    private TextView id, lastused, language, title;
    public static CardAdapter adapter;
    private ProgressDialog loadingDialog;
    private ListView cards_listview;
    private TextToSpeech tts;
    private LinearLayout nocards_lo;
    private MediaPlayer mediaPlayer;
    private boolean ttsReadable = false;
    private boolean offline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deck);

        context = this;
        
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cards_listview = (ListView) findViewById(R.id.cards_listview);
        nocards_lo = (LinearLayout) findViewById(R.id.nocards_lo);

        Intent intent = getIntent();
        deckId = intent.getIntExtra("deckId", -1);
        setView();
        //offline = !isOnline();
        //loading();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.deck, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.add:
                Intent intent = new Intent(this, AddWordActivity.class);
                intent.putExtra("deckId", deckId);
                startActivity(intent);
                return super.onOptionsItemSelected(item);
            case R.id.edit:
                Intent intent2 = new Intent(this, ManageDeckActivity.class);
                startActivity(intent2);
                return super.onOptionsItemSelected(item);
            case R.id.share:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogv = inflater.inflate(R.layout.dialog_share, null);
                TextView deck_tv = (TextView) dialogv.findViewById(R.id.id_tv);
                deck_tv.setText(String.valueOf(deck.getDeckId()));
                builder.setView(dialogv);
                builder.setNeutralButton(R.string.share_others, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent sendIntent = new Intent();
                        sendIntent.setAction(Intent.ACTION_SEND);
                        sendIntent.putExtra(Intent.EXTRA_TEXT, getResources().getText(R.string.share_text) + "\n" +
                                getString(R.string.share_scheme) + "://" + getString(R.string.share_host) + getString(R.string.share_pathPrefix) + "?deckId=" + deckId);
                        sendIntent.setType("text/plain");
                        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.share_via)));
                    }
                })
                .setNegativeButton(R.string.share_done, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.create().show();
                return super.onOptionsItemSelected(item);
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*@Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }
        super.onDestroy();
    }*/

    public void setView() {
        ParseQuery<Deck> innerQuery = Deck.getQuery();
        innerQuery.whereEqualTo("deckId", deckId);
        innerQuery.fromLocalDatastore();
        ParseQuery<Card> query = Card.getQuery();
        query.whereMatchesQuery("deck", innerQuery);
        query.whereLessThanOrEqualTo("display", new Date());
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Card>() {
            @Override
            public void done(List<Card> mcards, ParseException e) {
                if (mcards == null || mcards.size() == 0) {
                    nocards_lo.setVisibility(View.VISIBLE);
                    cards_listview.setVisibility(View.GONE);
                    Log.d("Deck", "No cards");
                } else if (e == null) {
                    cards = mcards;
                    adapter = new CardAdapter(DeckActivity.this, 0, cards, null, false);
                    cards_listview.setAdapter(adapter);
                } else {
                    Log.d("Deck", "Error: " + e.getMessage());
                }

            }
        });
        innerQuery.getFirstInBackground(new GetCallback<Deck>() {
            @Override
            public void done(Deck mdeck, ParseException e) {
                if (e == null) {
                    deck = mdeck;
                    if (deck.getDeckName2() == null) toolbar.setTitle(deck.getDeckName());
                    else toolbar.setTitle(deck.getDeckName() + " " + deck.getDeckName2());
                } else
                    Log.d("Deck", "Error: " + e.getMessage());
            }
        });
        /*deck = DecksActivity.decks.get(position);
        deckName = deck.getDeckName();
        forLan = deck.getForeignLan();
        localLan = deck.getLocalLan();
        Log.d("deckId", String.valueOf(deckId));
        ParseQuery<Deck> deckQuery = ParseQuery.getQuery(Deck.class);
        deckQuery.whereEqualTo("deckId", deckId);
        if (offline) deckQuery.fromLocalDatastore();
        try {
            deck = deckQuery.find().get(0);
            deckName = deck.getDeckName();
            forLan = deck.getForeignLan();
            localLan = deck.getLocalLan();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date current = new Date();
        ParseQuery<Card> cardQuery = ParseQuery.getQuery(Card.class);
        cardQuery.whereEqualTo("deck", deck);
        cardQuery.whereLessThanOrEqualTo("display", current);
        if (offline) cardQuery.fromPin(deckName);
        /*cardQuery.findInBackground(new FindCallback<Card>() {
            @Override
            public void done(List<Card> mCards, ParseException e) {
                cards = mCards;
                if (!offline) {
                    Card.unpinAllInBackground("cards", new DeleteCallback() {
                        @Override
                        public void done(ParseException e) {
                            Card.pinAllInBackground("cards", cards);
                        }
                    });
                    language.post(new Runnable() {
                        @Override
                        public void run() {
                            language.setText(forLan + " -> " + localLan);
                        }
                    });
                    title.post(new Runnable() {
                        @Override
                        public void run() {
                            title.setText(deckName);
                        }
                    });
                    lastused.post(new Runnable() {
                        @Override
                        public void run() {
                            SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
                            Date date = deck.getLastUsed();
                            lastused.setText("Last used: " + ft.format(date));
                        }
                    });
                    cards_listview.post(new Runnable() {
                        @Override
                        public void run() {
                            adapter = new CardAdapter(DeckActivity.this, 0, cards, null);
                            cards_listview.setAdapter(adapter);
                            cards_listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                                @Override
                                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                                    final Card selectedCard = cards.get(position);
                                    AlertDialog.Builder builder = new AlertDialog.Builder(DeckActivity.context);
                                    builder.setItems(R.array.card_option, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case 0:
                                                    break;
                                                case 1:
                                                    deleteCard(selectedCard);
                                                    break;
                                                case 2:
                                                    read(selectedCard);
                                                    break;
                                            }
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    return false;
                                }
                            });
                        }
                    });
                    loadingDialog.dismiss();
                }
            }
        });
        try {
            cards = cardQuery.find();
            if (!offline) {
                Card.unpinAllInBackground(deckName, new DeleteCallback() {
                    @Override
                    public void done(ParseException e) {
                        Card.pinAllInBackground(deckName, cards);
                    }
                });
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        language.post(new Runnable() {
            @Override
            public void run() {
                language.setText(forLan + "->" + localLan);
            }
        });
        title.post(new Runnable() {
            @Override
            public void run() {
                title.setText(deckName);
            }
        });
        lastused.post(new Runnable() {
            @Override
            public void run() {
                SimpleDateFormat ft = new SimpleDateFormat("yyyy/MM/dd");
                Date date = deck.getLastUsed();
                lastused.setText("Last used: " + ft.format(date));
            }
        });
        cards_listview.post(new Runnable() {
            @Override
            public void run() {
                adapter = new CardAdapter(DeckActivity.this, 0, cards, null);
                cards_listview.setAdapter(adapter);
                cards_listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        final Card selectedCard = cards.get(position);
                        AlertDialog.Builder builder = new AlertDialog.Builder(DeckActivity.context);
                        builder.setItems(R.array.card_option, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case 0:
                                        break;
                                    case 1:
                                        deleteCard(selectedCard);
                                        break;
                                    case 2:
                                        read(selectedCard);
                                        break;
                                }
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                        return false;
                    }
                });
            }
        });*/
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                loading();
            }
        }
        else if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                loading();
            }
        }
    }

    public void addCard(View view) {
        Intent intent = new Intent(this, AddWord1Activity.class);
        intent.putExtra("deckId", deckId);
        intent.putExtra("deckName", deckName);
        intent.putExtra("forLan", forLan);
        intent.putExtra("localLan", localLan);
        startActivityForResult(intent, 0);
    }
    */
    public void learn(View view) {
        if (cards.size()==0) {
            Toast.makeText(this, R.string.deck_nocards, Toast.LENGTH_LONG).show();
            return;
        }
        CharSequence[] fromto = new CharSequence[2];
        fromto[0] =  deck.getForeignLan() + "  ► " + deck.getLocalLan();
        fromto[1] = deck.getLocalLan() + " ► " + deck.getForeignLan();
        AlertDialog.Builder builder = new AlertDialog.Builder(DeckActivity.context);
        builder.setTitle(R.string.deck_select_title);
        builder.setItems(fromto, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(DeckActivity.context, FlashcardActivity.class);
                if (which == 0)
                    intent.putExtra("baselocal", false);
                else
                    intent.putExtra("baselocal", true);
                intent.putExtra("deckId", deckId);
                intent.putExtra("forLan", deck.getForeignLan());
                startActivityForResult(intent, 1);
            }
        });
        builder.create().show();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Log.d("ActivityResult: ", "Recieved");
            adapter.notifyDataSetChanged();
        }
    }

    public void play(View view) {
        if (cards.size()==0) {
            Toast.makeText(this, R.string.deck_nocards, Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, PlayActivity.class);
        intent.putExtra("deckId", deckId);
        startActivity(intent);
    }

    public void listen(View view) {
        if (cards.size()==0) {
            Toast.makeText(this, R.string.deck_nocards, Toast.LENGTH_LONG).show();
            return;
        }
        Intent intent = new Intent(this, ListenActivity.class);
        intent.putExtra("deckId", deckId);
        startActivity(intent);
    }
    /*
    public void delete(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_deck_msg)
                .setTitle(R.string.delete_deck_title)
                .setPositiveButton(R.string.delete_warning_agree, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deck.deleteInBackground();
                        Intent intent = new Intent();
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                })
                .setNegativeButton(R.string.delete_warning_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void unhide(View view) {
        loadingDialog = ProgressDialog.show(context, null, "Loading...", true, false);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ParseQuery<Card> query = ParseQuery.getQuery(Card.class);
                query.whereEqualTo("deck", deck);
                if (offline) query.fromLocalDatastore();
                Date current = new Date();
                try {
                    cards = query.find();
                    Card.pinAllInBackground(deck.getDeckName(), cards);
                    for (Card card : cards) {
                        card.setDisplay(current);
                        card.saveEventually();
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                setView();
                loadingDialog.dismiss();
            }
        });
        thread.start();
    }

    public void loading() {
        final Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                setView();
                Date current = new Date();
                deck.setLastUsed(current);
                deck.saveEventually();
                loadingDialog.dismiss();
                init();
            }
        });
        loadingDialog = new ProgressDialog(DeckActivity.context);
        loadingDialog.setMessage(getResources().getString(R.string.dialog_loading));
        loadingDialog.setCancelable(false);
        if (!offline)
            loadingDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getResources().getString(R.string.dialog_cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    offline = true;
                    thread.interrupt();
                    setView();
                    Date current = new Date();
                    deck.setLastUsed(current);
                    deck.saveEventually();
                    loadingDialog.dismiss();
                    init();
                }
            });
        loadingDialog.show();
        thread.start();
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void deleteCard(Card selectedcard) {
        final Card sCard = selectedcard;
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_card_msg)
                .setTitle(R.string.delete_card_title)
                .setPositiveButton(R.string.delete_warning_agree, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sCard.deleteInBackground();
                        loading();
                    }
                })
                .setNegativeButton(R.string.delete_warning_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

	public void read(Card card) {
        String text = card.getForeign();
        if (ttsReadable) {
            Log.d("Reading by TTS", text);
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
        } else {
            if(isOnline()) {
                try {
                    String url = "http://translate.google.com/translate_tts";
                    url += "?ie=UTF-8&tl=";
                    url += lanCode;
                    url += "&q=";
                    url += text;
                    mediaPlayer = new MediaPlayer();
                    mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
                    mediaPlayer.setDataSource(url);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else{
                Toast.makeText(DeckActivity.context, R.string.deck_read_internet, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void init() {
        lanCode = forLan.substring(0, 2).toLowerCase();
        final Locale local = new Locale(lanCode);
        tts = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                Log.d("Status", String.valueOf(status));
                if (tts.isLanguageAvailable(local) >= 0) {
                    tts.setLanguage(local);
                    tts.setSpeechRate(0.5f);
                    ttsReadable = true;
                }
            }
        });
    }*/
}
