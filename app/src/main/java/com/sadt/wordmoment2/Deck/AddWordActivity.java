package com.sadt.wordmoment2.Deck;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.SaveCallback;
import com.sadt.wordmoment2.Decks.DecksFragment;
import com.sadt.wordmoment2.LoadSuggestion;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AddWordActivity extends ActionBarActivity {
    private Toolbar toolbar;
    public static Context context;
    public static EditText word_et, meaning_et;
    private Spinner decks_sp;
    private List<Deck> decks;
    private Deck deck;
    public static ListView suggestion_list;
    public static ProgressBar progressBar;
    private LinearLayout suggestion_lo;
    public static int added;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_word);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        word_et = (EditText) findViewById(R.id.word_et);
        meaning_et = (EditText) findViewById(R.id.meaning_et);
        decks_sp = (Spinner) findViewById(R.id.decks_sp);
        suggestion_list = (ListView) findViewById(R.id.suggestion_list);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        suggestion_lo = (LinearLayout) findViewById(R.id.suggestion_lo);

        added = 0;

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        decks = DecksFragment.decks;
        List<String> sDecks = new ArrayList<>();
        for (Deck deck: decks){
            if (deck.getDeckName2() == null)
                sDecks.add(deck.getDeckName());
            else
                sDecks.add(deck.getDeckName()+" "+deck.getDeckName2());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sDecks);
        decks_sp.setAdapter(adapter);

        deck = DeckActivity.deck;
        for (int i=0; i<decks.size(); i++){
            if (decks.get(i) == deck){
                decks_sp.setSelection(i);
                break;
            }
        }

        word_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                LoadSuggestion();
            }
        });

        meaning_et.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                LoadSuggestion();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.add_word, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
            case R.id.done:
                add();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void LoadSuggestion(){
        String word = word_et.getText().toString();
        String meaning = meaning_et.getText().toString();

        if(word.trim().equals("") && meaning.trim().equals(""))
            return;

        if(!word.trim().equals("") && !meaning.trim().equals(""))
            return;

        suggestion_lo.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        suggestion_list.setVisibility(View.GONE);

        if (!word.trim().equals("") && meaning.trim().equals(""))
            new LoadSuggestion().execute(deck.getForeignLan(), deck.getLocalLan(), word, "fromfor");
        else if (word.trim().equals("") && !meaning.trim().equals(""))
            new LoadSuggestion().execute(deck.getLocalLan(), deck.getForeignLan(), meaning, "fromlocal");
    }

    public void add() {
        String word = word_et.getText().toString();
        String meaning = meaning_et.getText().toString();

        if(word.trim().equals("") && meaning.trim().equals(""))
            return;

        word = word.substring(0, 1).toUpperCase() + word.substring(1);
        meaning = meaning.substring(0, 1).toUpperCase() + meaning.substring(1);
        deck = DeckActivity.deck;
        Card card = new Card();
        card.setDeck(deck);
        card.setLocal(meaning);
        card.setForeign(word);
        Date current = new Date();
        card.setDisplay(current);
        card.pinInBackground();
        card.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    added++;
                    Toast.makeText(AddWordActivity.context, String.valueOf(added)+" "+getString(R.string.addword_success)+" "+deck.getDeckName(), Toast.LENGTH_LONG).show();
                    word_et.setText("");
                    meaning_et.setText("");
                } else {
                    Toast.makeText(AddWordActivity.context, getString(R.string.addword_fail) + " "+deck.getDeckName(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
