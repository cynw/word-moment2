package com.sadt.wordmoment2.Deck;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.sadt.wordmoment2.Decks.DecksFragment;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManageDeckActivity extends AppCompatActivity {
    private Toolbar toolbar;
    public static Context context;
    private List<Card> cards;
    private Deck deck;
    private CardAdapter adapter;
    private ListView cards_listview;
    private Boolean all;
    private TextView selectall_text, count_tv, hide_tv;
    private ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_deck);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        cards_listview = (ListView) findViewById(R.id.cards_listview);
        selectall_text = (TextView) findViewById(R.id.selectall_text);
        count_tv =  (TextView) findViewById(R.id.count_tv);
        hide_tv = (TextView) findViewById(R.id.hide_tv);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        cards = DeckActivity.cards;
        deck = cards.get(0).getDeck();

        if(deck.getDeckName2()==null) getSupportActionBar().setTitle(deck.getDeckName());
        else getSupportActionBar().setTitle(deck.getDeckName()+" "+deck.getDeckName2());

        adapter = new CardAdapter(ManageDeckActivity.context, 0, cards, null, true);
        cards_listview.setAdapter(adapter);
        cards_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                adapter.setChecked(position);
                adapter.notifyDataSetChanged();
                int count = adapter.getChecked().size();
                if (count>0){
                    count_tv.setText("(" + String.valueOf(count) + ")");
                    hide_tv.setText(R.string.managedeck_hide);
                }
                else
                    init();
            }
        });

        all = false;
    }

    public void init(){
        count_tv.setText("");
        hide_tv.setText(R.string.managedeck_unhide);
        all=false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.manage_deck, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.done) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void selectall(View v){
        if (all) {
            adapter.setAllChecked(false);
            adapter.notifyDataSetChanged();
            selectall_text.setText(R.string.managedeck_all);
            init();
        }
        else {
            adapter.setAllChecked(true);
            adapter.notifyDataSetChanged();
            selectall_text.setText(R.string.managedeck_none);
            all = true;
            int count = adapter.getChecked().size();
            count_tv.setText("(" + String.valueOf(count) + ")");
            hide_tv.setText(R.string.managedeck_hide);
        }
    }

    public void hide(View v){
        List<Card> cards = adapter.getChecked();
        int count = cards.size();
        if (count>0){
            Date today = new Date();
            Date tomorrow = new Date(today.getTime() + (1000 * 60 * 60 * 24));
            for (Card card: cards) {
                card.setDisplay(tomorrow);
                adapter.remove(card);
                DeckActivity.adapter.remove(card);
            }
            adapter.setAllChecked(false);
            adapter.notifyDataSetChanged();
            Card.saveAllInBackground(cards, new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e==null)
                        Toast.makeText(ManageDeckActivity.context, R.string.managedeck_hide_success, Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(ManageDeckActivity.context, R.string.managedeck_hide_fail, Toast.LENGTH_LONG).show();
                    init();
                }
            });
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
        }
        else{
            loadingDialog = new ProgressDialog(ManageDeckActivity.context);
            loadingDialog = ProgressDialog.show(context, null, getString(R.string.dialog_loading), true, false);
            ParseQuery<Card> query = ParseQuery.getQuery(Card.class);
            query.whereEqualTo("deck", deck);
            query.whereGreaterThan("display", new Date());
            query.findInBackground(new FindCallback<Card>() {
                @Override
                public void done(List<Card> mcards, ParseException e) {
                    if (e != null) {
                        Toast.makeText(ManageDeckActivity.context, R.string.managedeck_unhide_fail, Toast.LENGTH_LONG).show();
                    } else {
                        Date current = new Date();
                        for (Card card : mcards)
                            card.setDisplay(current);
                        adapter.addAll(mcards);
                        DeckActivity.adapter.addAll(mcards);
                        adapter.notifyDataSetChanged();
                        loadingDialog.dismiss();
                        Card.saveAllInBackground(mcards);
                    }
                }
            });
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
        }
    }

    public void move(View v){
        final List<Card> scards = adapter.getChecked();
        if (scards.size() == 0){
            Toast.makeText(ManageDeckActivity.context, R.string.managedeck_noselected, Toast.LENGTH_LONG).show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogv = inflater.inflate(R.layout.dialog_movecard, null);
        final Spinner decks_sp = (Spinner) dialogv.findViewById(R.id.decks_sp);
        List<Deck> decks = DecksFragment.decks;
        List<String> sDecks = new ArrayList<>();
        for (Deck deck: decks){
            if (deck.getDeckName2() == null)
                sDecks.add(deck.getDeckName());
            else
                sDecks.add(deck.getDeckName()+" "+deck.getDeckName2());
        }
        final ArrayAdapter<String> sadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sDecks);
        decks_sp.setAdapter(sadapter);
        builder.setView(dialogv);
        builder.setPositiveButton(R.string.managedeck_move_move, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Deck deck = DecksFragment.decks.get(decks_sp.getSelectedItemPosition());
                for (Card mcard: scards) {
                    mcard.setDeck(deck);
                    adapter.remove(mcard);
                    DeckActivity.adapter.remove(mcard);
                }
                Card.saveAllInBackground(scards, new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e==null) Toast.makeText(ManageDeckActivity.context, R.string.managedeck_move_success, Toast.LENGTH_LONG).show();
                        else Toast.makeText(ManageDeckActivity.context, R.string.managedeck_move_fail, Toast.LENGTH_LONG).show();
                    }
                });
                adapter.notifyDataSetChanged();
                adapter.setAllChecked(false);
                init();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
            }
        });
        builder.setNegativeButton(R.string.managedeck_move_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }

    public void delete(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.delete_card_msg)
                .setTitle(R.string.delete_card_title)
                .setPositiveButton(R.string.delete_warning_agree, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        final List<Card> scards = adapter.getChecked();
                        ParseObject.deleteAllInBackground(scards, new DeleteCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e==null) {
                                    Toast.makeText(ManageDeckActivity.context, R.string.managedeck_delete_successs, Toast.LENGTH_LONG).show();
                                    for (Card card: scards) {
                                        adapter.remove(card);
                                        DeckActivity.adapter.remove(card);
                                    }
                                    adapter.setAllChecked(false);
                                    adapter.notifyDataSetChanged();
                                    init();
                                    Intent intent = new Intent();
                                    setResult(RESULT_OK, intent);
                                }
                                else
                                    Toast.makeText(ManageDeckActivity.context, R.string.managedeck_delete_fail, Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                })
                .setNegativeButton(R.string.delete_warning_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
