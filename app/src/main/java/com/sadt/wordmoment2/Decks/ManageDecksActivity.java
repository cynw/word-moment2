package com.sadt.wordmoment2.Decks;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.sadt.wordmoment2.ExpandableListAdapter;
import com.sadt.wordmoment2.LanguageActivity;
import com.sadt.wordmoment2.LoadDeckActivity;
import com.sadt.wordmoment2.MainActivity;
import com.sadt.wordmoment2.Deck.ManageDeckActivity;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ManageDecksActivity extends ActionBarActivity {
    private Toolbar toolbar;
    public static Context context;
    private ExpandableListView expandableListView;
    private ExpandableListAdapter adapter;
    private List<Deck> decks;
    public static TextView count_tv;
    private LinearLayout nodecks_lo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_decks);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        count_tv = (TextView) findViewById(R.id.count_tv);
        nodecks_lo = (LinearLayout) findViewById(R.id.nodecks_lo);
        expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        decks = DecksFragment.decks;
        if (decks == null || decks.size()==0) {
            nodecks_lo.setVisibility(View.VISIBLE);
            expandableListView.setVisibility(View.GONE);
            Log.d("Decks", "No decks");
        }
        else {
            HashMap<String, List<Deck>> expandableListDetail = new HashMap<String, List<Deck>>();

            String foreign = decks.get(0).getForeignLan();
            List<Deck> a = new ArrayList<Deck>();
            for (Deck deck : decks) {
                if (!deck.getForeignLan().equals(foreign)) {
                    expandableListDetail.put(foreign, a);
                    foreign = deck.getForeignLan();
                    a = new ArrayList<Deck>();
                }
                a.add(deck);
            }
            expandableListDetail.put(a.get(0).getForeignLan(), a);

            expandableListView = (ExpandableListView) findViewById(R.id.expandableListView);
            adapter = new ExpandableListAdapter(ManageDecksActivity.context, new ArrayList<>(expandableListDetail.keySet()), expandableListDetail, true);
            expandableListView.setAdapter(adapter);

            for (int i = 0; i < adapter.getGroupCount(); i++)
                expandableListView.expandGroup(i);
        }
        /*expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ExpandableListAdapter.ViewHolder view = (ExpandableListAdapter.ViewHolder) v.getTag();
                adapter.setChecked(view.getDeck());
                return false;
            }
        });*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.manage_decks, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.done) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void adddeck(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(ManageDecksActivity.context);
        builder.setTitle(R.string.add_deck_dialog_title)
                .setItems(R.array.add_deck_dialog_items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            dialog.dismiss();
                            Intent intent = new Intent(ManageDecksActivity.context, AddDeckActivity.class);
                            startActivity(intent);
                        } else if (which == 1) {
                            dialog.dismiss();
                            Intent intent = new Intent(MainActivity.context, LoadDeckActivity.class);
                            intent.putExtra("deckId", "none");
                            startActivity(intent);
                        } else if (which == 2) {
                            dialog.dismiss();
                            Intent intent = new Intent(MainActivity.context, LanguageActivity.class);
                            intent.putExtra("buy", false);
                            startActivity(intent);
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void merge(View v){
        final List<Deck> sdecks = adapter.getChecked();
        if (sdecks.size() == 0){
            Toast.makeText(ManageDeckActivity.context, R.string.decks_noselected, Toast.LENGTH_LONG).show();
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogv = inflater.inflate(R.layout.dialog_mergedeck, null);
        final Spinner decks_sp = (Spinner) dialogv.findViewById(R.id.decks_sp);
        List<String> sDecks = new ArrayList<>();
        for (Deck deck: sdecks){
            if (deck.getDeckName2() == null)
                sDecks.add(deck.getDeckName());
            else
                sDecks.add(deck.getDeckName()+" "+deck.getDeckName2());
        }
        final ArrayAdapter<String> sadapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, sDecks);
        decks_sp.setAdapter(sadapter);
        builder.setView(dialogv);
        builder.setPositiveButton(R.string.managedecks_merge_merge, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                    final Deck deck = sdecks.get(decks_sp.getSelectedItemPosition());
                for (Deck d : sdecks){
                    if (d == deck)
                        continue;
                    ParseQuery<Card> qcard = Card.getQuery();
                    qcard.whereEqualTo("deck",d);
                    qcard.fromLocalDatastore();
                    qcard.findInBackground(new FindCallback<Card>() {
                        @Override
                        public void done(List<Card> cards, ParseException e) {
                            for (Card c : cards) {
                                c.setDeck(deck);
                                c.saveEventually();
                            }
                        }
                    });
                    d.deleteEventually();
                    adapter.remove(d);
                    DecksFragment.adapter.remove(d);
                }
                adapter.notifyDataSetChanged();
                DecksFragment.adapter.notifyDataSetChanged();
                adapter.setAllChecked(false);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
            }
        });
        builder.setNegativeButton(R.string.managedecks_merge_cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.create().show();
    }

    public void delete(View v){
        final List<Deck> sdecks = adapter.getChecked();
        if (sdecks.size() == 0){
            Toast.makeText(this, R.string.decks_noselected, Toast.LENGTH_SHORT).show();
        }
        else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage(R.string.decks_delete_msg)
                    .setTitle(R.string.decks_delete_title)
                    .setPositiveButton(R.string.decks_delete_yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            count_tv.setText("");
                            DecksFragment.decks.removeAll(sdecks);
                            for (Deck mdeck : sdecks) {
                                adapter.remove(mdeck);
                                DecksFragment.adapter.remove(mdeck);
                            }

                            Deck.deleteAllInBackground(sdecks, new DeleteCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null)
                                        Toast.makeText(ManageDecksActivity.context, R.string.managedecks_delete_success, Toast.LENGTH_LONG).show();
                                    else
                                        Toast.makeText(ManageDecksActivity.context, R.string.managedecks_delete_fail, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    })
                    .setNegativeButton(R.string.decks_delete_no, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
}
