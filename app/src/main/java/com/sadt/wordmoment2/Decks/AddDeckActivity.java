package com.sadt.wordmoment2.Decks;

import java.util.Date;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;


public class AddDeckActivity extends ActionBarActivity {
    public static Context context;
    private Toolbar toolbar;
    private EditText deckname1_et, deckname2_et;
    private RadioButton newdeck_radio, existing_radio;
    private ProgressDialog loadingDialog;
    private Spinner decklan1_sp, decklan2_sp;
    private ProgressBar loading_deck;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_deck);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        deckname1_et = (EditText) findViewById(R.id.deckname1_et);
        deckname2_et = (EditText) findViewById(R.id.deckname2_et);
        decklan1_sp = (Spinner) findViewById(R.id.decklan1_sp);
        decklan2_sp = (Spinner) findViewById(R.id.decklan2_sp);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.from_lan_display));
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.display_lan));
        decklan1_sp.setAdapter(adapter1);
        decklan2_sp.setAdapter(adapter2);

        //newdeck_radio = (RadioButton) findViewById(R.id.newdeck_radio);
        //existing_radio = (RadioButton) findViewById(R.id.existing_radio);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.adddeck, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.done:
                addDeck();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public String eng_lan(String forlan){
        String[] languegs = getResources().getStringArray(R.array.languages);
        String[] display_lan = getResources().getStringArray(R.array.display_lan);

        for (int i=0; i<languegs.length; i++){
            if (forlan.equals(display_lan[i]))
                return languegs[i];
        }
        return "";
    }

    public void addDeck() {
        String[] eng_lan = getResources().getStringArray(R.array.languages);
        String lan1 = eng_lan(decklan1_sp.getSelectedItem().toString());
        String lan2 = eng_lan[decklan2_sp.getSelectedItemPosition()];
        String name1 = deckname1_et.getText().toString();
        String name2 = deckname2_et.getText().toString();

        if(lan1.equals("-") || lan2.equals("-") || lan1==lan2){
            Toast.makeText(AddDeckActivity.context, R.string.adddeck_checklan, Toast.LENGTH_LONG).show();
            return;
        }
        if((name1==null || name1.trim().equals(""))&& (name2==null || name2.trim().equals(""))){
            Toast.makeText(AddDeckActivity.context, R.string.adddeck_checkname, Toast.LENGTH_LONG).show();
            return;
        }
        ParseUser me = ParseUser.getCurrentUser();
        Deck deck = new Deck();
        deck.setDeckName(name1);
        deck.setDeckName2(name2);
        deck.setLocalLan(lan1);
        deck.setForeignLan(lan2);
        deck.setUser(me);
        deck.setDeckId();
        deck.setLastIndex(0);
        deck.setLastUsed(new Date());
        deck.pinInBackground();
        deck.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null)
                    Toast.makeText(AddDeckActivity.context, R.string.adddeck_success, Toast.LENGTH_LONG).show();
                else
                    Toast.makeText(AddDeckActivity.context, R.string.adddeck_fail, Toast.LENGTH_LONG).show();
            }
        });
        finish();
        //ParsePush.subscribeInBackground(foreignLan);
        /*if (newdeck_radio.isChecked()) {
            if (new_deck.getText().toString().trim().equals("")) {
                Toast.makeText(this, R.string.noname_warning, Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(this, LanguageActivity.class);
                intent.putExtra("deckName", new_deck.getText().toString());
                startActivity(intent);
            }
        } else {
            if (new_deck.getText().toString().trim().equals("")) {
                Toast.makeText(this, R.string.noname_warning, Toast.LENGTH_SHORT).show();
            } else {
                loadExistingDeck(Integer.valueOf(new_deck.getText().toString()));
            }
        }*/
    }

    /*@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("DEF", "a");
        if (requestCode == 0) {
            Log.d("DEF", "b");
            if (resultCode == RESULT_OK) {
                Log.d("DEF", "c");
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    public void addDeck(View view) {
        if (newdeck_radio.isChecked()) {
            if (new_deck.getText().toString().trim().equals("")) {
                Toast.makeText(this, R.string.noname_warning, Toast.LENGTH_SHORT).show();
            } else {
                Intent intent = new Intent(this, LanguageActivity.class);
                intent.putExtra("deckName", new_deck.getText().toString());
                startActivity(intent);
            }
        } else {
            if (new_deck.getText().toString().trim().equals("")) {
                Toast.makeText(this, R.string.noname_warning, Toast.LENGTH_SHORT).show();
            } else {
                loadExistingDeck(Integer.valueOf(new_deck.getText().toString()));
            }
        }
    }

    public void newdeck_radio(View view) {
        existing_radio.setChecked(false);
        newdeck_radio.setChecked(true);
        existing_deck.setText("");
        new_deck.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(new_deck, InputMethodManager.SHOW_IMPLICIT);
    }

    public void existing_radio(View view) {
        newdeck_radio.setChecked(false);
        existing_radio.setChecked(true);
        new_deck.setText("");
        existing_deck.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(existing_deck, InputMethodManager.SHOW_IMPLICIT);
    }

    public void cancel(View view) {
        finish();
    }

    public void loadExistingDeck(int deckId) {
        final int queryId = deckId;
        loadingDialog = ProgressDialog.show(AddDeckActivity.context, null, "Loading...", true, false);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                List<Deck> decks = null;
                ParseQuery<Deck> deckQuery = ParseQuery.getQuery(Deck.class);
                deckQuery.whereEqualTo("deckId", queryId);
                try {
                    decks = deckQuery.find();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                loadingDialog.dismiss();
                if (decks.size() == 0) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AddDeckActivity.context, R.string.nodeck_warning, Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    final Deck deck = decks.get(0);
                    AlertDialog.Builder builder = new AlertDialog.Builder(AddDeckActivity.context);
                    builder.setMessage(R.string.adddeck_msg)
                            .setTitle(deck.getDeckName())
                            .setPositiveButton(R.string.adddeck_yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    loadingDialog = ProgressDialog.show(AddDeckActivity.context, null, "Loading...", true, false);
                                    Thread thread = new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Deck newdeck = new Deck();
                                            newdeck.setDeckId();
                                            newdeck.setDeckName(deck.getDeckName());
                                            newdeck.setUser(ParseUser.getCurrentUser());
                                            newdeck.setForeignLan(deck.getForeignLan());
                                            newdeck.setLocalLan(deck.getLocalLan());
                                            newdeck.setLastIndex(0);
                                            newdeck.setLastUsed(new Date());
                                            newdeck.saveEventually();
                                            ParseQuery<Card> cardQuery = ParseQuery.getQuery(Card.class);
                                            cardQuery.whereEqualTo("deck", deck);
                                            try {
                                                List<Card> cards = cardQuery.find();
                                                for (Card card : cards) {
                                                    Card newcard = new Card();
                                                    newcard.setDeck(newdeck);
                                                    newcard.setDisplay(new Date());
                                                    newcard.setForeign(card.getForeign());
                                                    newcard.setLocal(card.getLocal());
                                                    newcard.saveEventually();
                                                }
                                            } catch (ParseException e) {
                                                e.printStackTrace();
                                            }
                                            loadingDialog.dismiss();
                                        }
                                    });
                                    thread.start();
                                }
                            })
                            .setNegativeButton(R.string.adddeck_no, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
        thread.start();
    }

    public void getSuggestDeck(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Language");
                try {
                    final List<ParseObject> languages = query.find();
                    adapter = new LanguageAdapter(AddDeckActivity.context, 0, languages);
                    suggestlanguage_listview.post(new Runnable() {
                        @Override
                        public void run() {
                            suggestlanguage_listview.setAdapter(adapter);
                            suggestlanguage_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent intent = new Intent(AddDeckActivity.context, SuggestDeckActivity.class);
                                    intent.putExtra("language", languages.get(position).getString("language"));
                                    startActivityForResult(intent, 0);
                                }
                            });
                        }
                    });
                    loading_deck.post(new Runnable() {
                        @Override
                        public void run() {
                            loading_deck.setVisibility(View.GONE);
                        }
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    private boolean isOnline() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }*/
}
