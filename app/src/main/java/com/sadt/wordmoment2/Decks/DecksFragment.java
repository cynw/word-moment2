package com.sadt.wordmoment2.Decks;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.sadt.wordmoment2.Deck.DeckActivity;
import com.sadt.wordmoment2.ExpandableListAdapter;
import com.sadt.wordmoment2.MainActivity;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.LinearLayout;

public class DecksFragment extends Fragment {
	private ExpandableListView expandableListView;
	public static ExpandableListAdapter adapter;
	private LinearLayout nodecks_lo;
	private ParseUser me;
	private View rootView;
	public static List<Deck> decks;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_decks, container, false);

		expandableListView = (ExpandableListView) rootView.findViewById(R.id.expandableListView);
		nodecks_lo = (LinearLayout) rootView.findViewById(R.id.nodecks_lo);

		me = ParseUser.getCurrentUser();
		ParseQuery<Deck> deckQuery = Deck.getQuery();
        deckQuery.whereEqualTo("user", me);
        deckQuery.orderByAscending("foreignLan");
        deckQuery.fromLocalDatastore();
		deckQuery.findInBackground(new FindCallback<Deck>(){

			@Override
			public void done(List<Deck> mdecks, ParseException e) {
				
				if (mdecks == null || mdecks.size()==0) {
					nodecks_lo.setVisibility(View.VISIBLE);
					expandableListView.setVisibility(View.GONE);
					Log.d("Decks", "No decks");
				}
				else if (e == null) {
					decks = mdecks;
					HashMap<String, List<Deck>> expandableListDetail = new HashMap<String, List<Deck>>();
					
					String foreign = decks.get(0).getForeignLan();
					List<Deck> a = new ArrayList<Deck>();
		            for (Deck deck : decks){
		            	if (!deck.getForeignLan().equals(foreign)){
		            		expandableListDetail.put(foreign, a);
		            		foreign = deck.getForeignLan();
		            		a = new ArrayList<>();
		            	}
		            	a.add(deck);
		            }
		            expandableListDetail.put(a.get(0).getForeignLan(), a);

		    		adapter = new ExpandableListAdapter(MainActivity.context, new ArrayList<String>(expandableListDetail.keySet()), expandableListDetail, false);
		            expandableListView.setAdapter(adapter);
		            expandableListView.setOnChildClickListener(new OnChildClickListener(){

						@Override
						public boolean onChildClick(ExpandableListView parent,
								View v, int groupPosition, int childPosition,
								long id) {
                            Log.d("ldrsjfr","tlidrjflrdsjd");
							Intent intent = new Intent(MainActivity.context, DeckActivity.class);
							Log.d("Decks","Deck id: "+ v.getTag(R.string.tag2));
							intent.putExtra("deckId", (Integer) v.getTag(R.string.tag2));
							startActivity(intent);
							return false;
						}
		            	
		            });
		            
		            for(int i=0; i < adapter.getGroupCount(); i++)
		            	expandableListView.expandGroup(i);
		        }
				else {
		            Log.d("Decks", "Error: " + e.getMessage());
		        }
				
			}
			
		});
        
		return rootView;
	}

}
