package com.sadt.wordmoment2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sadt.wordmoment2.Deck.CardAdapter;
import com.sadt.wordmoment2.Deck.DeckActivity;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;


public class ListenActivity extends AppCompatActivity implements TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {
    private static Context context;
    private final int READNONE = 0, READFOR = 1, READALL = 2;
    private TextView title, listen_for, listen_local;
    private ListView listen_listview;
    private ImageButton listen_play;
    private ProgressDialog loadingDialog;
    private String forLan, localLan, forCode, localCode;
    private int deckId, size;
    private Deck deck;
    private List<Card> cards;
    private CardAdapter adapter;
    private TextToSpeech tts;
    private int ttsReadable = 0;
    private boolean playing = false;
    private Locale foreign, local;
    private int readingIndex = 0;
    private Toolbar toolbar;
    private HashMap<String, String> myHashAlarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listen);

        Intent checkIntent = new Intent();
        checkIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        checkIntent.setPackage("com.google.android.tts");
        startActivityForResult(checkIntent, 0);

        context = this;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        listen_for = (TextView) findViewById(R.id.listen_for);
        listen_local = (TextView) findViewById(R.id.listen_local);
        listen_listview = (ListView) findViewById(R.id.listen_listview);
        listen_play = (ImageButton) findViewById(R.id.listen_play);

        deck = DeckActivity.deck;
        cards = DeckActivity.cards;
        forLan = deck.getForeignLan();
        localLan = deck.getLocalLan();
        size = cards.size();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (deck.getDeckName2()==null || deck.getDeckName2().trim().equals("")) getSupportActionBar().setTitle(deck.getDeckName());
        else getSupportActionBar().setTitle(deck.getDeckName() + " " + deck.getDeckName2());

        adapter = DeckActivity.adapter;
        listen_listview.setAdapter(adapter);

        setView();
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                tts = new TextToSpeech(this, this, "com.google.android.tts");
                //tts = new TextToSpeech(this, this);
            } else {
                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                installIntent.setPackage("com.google.android.tts");
                startActivity(installIntent);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();
    }

    public void setView() {
        listen_for.post(new Runnable() {
            @Override
            public void run() {
                listen_for.setText(cards.get(readingIndex).getForeign());
            }
        });
        listen_local.post(new Runnable() {
            @Override
            public void run() {
                listen_local.setText(cards.get(readingIndex).getLocal());
            }
        });
    }

    @Override
    public void onInit(int status) {
        forCode = lan_code(forLan);
        localCode = lan_code(localLan);
        foreign = new Locale(forCode);
        local = new Locale(localCode);
        ttsReadable = READNONE;
        tts.setSpeechRate(0.5f);
        if (tts.isLanguageAvailable(foreign) >= 0) {
            ttsReadable = READFOR;
            if (status == TextToSpeech.SUCCESS) {
                tts.setOnUtteranceCompletedListener(this);
                if (tts.isLanguageAvailable(local) >= 0)
                    ttsReadable = READALL;
            } else {
                Log.e("TTS", "Initilization Failed!");
            }
        }
        if(ttsReadable == READFOR)
            Toast.makeText(ListenActivity.context, R.string.listen_onlyfor, Toast.LENGTH_SHORT).show();
        else if(ttsReadable == READNONE) {
            Toast.makeText(ListenActivity.context, R.string.listen_cannot, Toast.LENGTH_LONG).show();
            finish();
        }
    }

    public void play(View view) {
        if (playing) {
            playing = false;
            tts.stop();
            listen_play.setImageResource(R.drawable.ic_play_arrow_black_48dp);
        } else {
            playing = true;
            listen_play.setImageResource(R.drawable.ic_pause_black_48dp);
            read();
        }
    }

    public void prev(View view) {
        readingIndex--;
        if (readingIndex < 0)
            readingIndex = size - 1;
        setView();
        if (playing)
            read();
        listen_listview.smoothScrollToPosition(readingIndex);
    }

    public void next(View view) {
        readingIndex++;
        if (readingIndex == size)
            readingIndex = 0;
        setView();
        if (playing) {
            Log.d("Playing", "true");
            read();
        }
        listen_listview.smoothScrollToPosition(readingIndex);
    }

    public void read() {
        myHashAlarm = new HashMap();
        myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "end of a word");
        if (ttsReadable == READFOR) {
            tts.setLanguage(foreign);
            tts.speak(cards.get(readingIndex).getForeign(), TextToSpeech.QUEUE_FLUSH, myHashAlarm);
        } else if (ttsReadable == READALL) {
            tts.setLanguage(foreign);
            tts.speak(cards.get(readingIndex).getForeign(), TextToSpeech.QUEUE_FLUSH, null);
            tts.setLanguage(local);
            tts.speak(cards.get(readingIndex).getLocal(), TextToSpeech.QUEUE_ADD, myHashAlarm);
        }
    }

    @Override
    public void onUtteranceCompleted(String utteranceId) {
        Log.d("TTS", "End of a word");
        next(getCurrentFocus());
    }

    public String lan_code(String eng){
        String[] languegs = getResources().getStringArray(R.array.languages);
        String[] lancodes = getResources().getStringArray(R.array.lan_code);

        for (int i=0; i<languegs.length; i++){
            if (eng.equals(languegs[i]))
                return lancodes[i];
        }
        return "";
    }
}
