package com.sadt.wordmoment2;

import android.app.Application;

import com.facebook.FacebookSdk;
import com.parse.Parse;
import com.parse.ParseObject;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;

public class FirstActivity extends Application {
	@Override
    public void onCreate() {
        super.onCreate();
        ParseObject.registerSubclass(Deck.class);
        ParseObject.registerSubclass(Card.class);
        //Parse.enableLocalDatastore(this);
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(getString(R.string.parse_app_id))
                .server(getString(R.string.parse_server))
                .enableLocalDataStore()
                .build()
        );
        /*Parse.initialize(this, getString(R.string.parse_app_id),
                getString(R.string.parse_client_key));*/
        FacebookSdk.sdkInitialize(getApplicationContext());
    }
}
