package com.sadt.wordmoment2;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseUser;


public class SummaryActivity extends ActionBarActivity {
    private TextView walnut_score, count_heading, count_text;
    private int correct, incorrect, skip;
    private List<Integer> answers;
    private Context context;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_summary);
        context =  this;

        walnut_score = (TextView) findViewById(R.id.walnut_score);
        count_heading = (TextView) findViewById(R.id.count_heading);
        count_text = (TextView) findViewById(R.id.count_text);
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        answers = PlayingActivity.answers;

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        for (int i = 0; i < answers.size(); i++) {
            if (answers.get(i) == 1)
                correct++;
            else if (answers.get(i) == -1)
                incorrect++;
            else
                skip++;
        }
        walnut_score.setText(String.valueOf(correct));
        count_heading.setText(getResources().getText(R.string.summary_total) + " " + String.valueOf(answers.size()) + " " + getResources().getText(R.string.summary_question));
        count_text.setText(getResources().getText(R.string.summary_correct) + " " + String.valueOf(correct) + " (" + String.valueOf(Math.round(correct * 100 / answers.size())) + "%)\n"
                + getResources().getText(R.string.summary_incorrect) + " " + String.valueOf(incorrect) + " (" + String.valueOf(Math.round(incorrect * 100 / answers.size())) + "%)\n"
                + getResources().getText(R.string.summary_skipped) + " " + String.valueOf(skip) + " (" + String.valueOf(Math.round(skip * 100 / answers.size())) + "%)");

        ParseUser.getCurrentUser().increment("walnut", correct);
        ParseUser.getCurrentUser().saveEventually();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    public void post_facebook(View view) {
       
    }

    public void done(View view) {
        finish();
    }

    public void see_answers(View view){
        Intent intent = new Intent(this, MyanswersActivity.class);
        startActivity(intent);
    }
}
