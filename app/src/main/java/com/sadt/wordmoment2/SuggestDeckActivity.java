package com.sadt.wordmoment2;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.util.IabHelper;
import com.sadt.wordmoment2.util.IabResult;
import com.sadt.wordmoment2.util.Inventory;
import com.sadt.wordmoment2.util.Purchase;

import java.util.List;


public class SuggestDeckActivity extends ActionBarActivity {
    public static Context context;
    private ListView suggestdeck_listview;
    private ProgressBar loading_deck;
    private String language;
    private ParseObject lan;
    private Toolbar toolbar;
    private Deck deck;  //new Deck
    private ProgressDialog loadingDialog;
    private List<ParseObject> decks;
    private IabHelper mHelper;
    private Boolean purchased;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggest_deck);

        Intent intent = getIntent();
        language = intent.getStringExtra("language");

        context = this;

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        suggestdeck_listview = (ListView) findViewById(R.id.suggestdeck_listview);
        loading_deck  = (ProgressBar) findViewById(R.id.loading_deck);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        getSupportActionBar().setTitle(language);

        String base64EncodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyFHCSWbpe8X6lGJgJcJ+EW74yv5FPbNZbXbKAq7ofv4+V2lnMqR4z7LdlkYS32SWkZ5IAE3+cpWr0oEo3jGTlInm+tWbbbhK5z4ZmmwYLrmacnbVSHi8d6FlZNWbHbiDeqjWRFXfpVogV5B2LMuBPXwl4LoD34Jl+ZxF9OzkS16qNXqrY/anx21xMQUU2o2qZ7H92PtB3zuxjmNYBaS+Aqb6E/nrU/ZSnbVD2r52JzLFXvZ6saA0JOcyDyHKfyX1MRPmflhswX/lX+wlCWY2O5Kgj/T7FESIqL5SpIl14dqjstTDfZDtPxU/fzHDCMQe7kFC4mVl69/aZDi14MUZTQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d("IAP", "Problem setting up In-app Billing: " + result);
                }
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });
    }

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d("IAP", "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                //complain("Failed to query inventory: " + result);
                return;
            }
            Log.d("IAP", "Query inventory was successful.");

            Purchase lanPurchase = inventory.getPurchase(language.toLowerCase());
            purchased = lanPurchase != null;

            loadUi();
        }
    };

    public void loadUi(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Language");
        query.whereEqualTo("language", language);
        query.getFirstInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject mlan, ParseException e) {
                lan = mlan;
                ParseQuery<ParseObject> query = ParseQuery.getQuery("SampleDeck");
                query.whereEqualTo("language", lan);
                query.addDescendingOrder("free");
                query.findInBackground(new FindCallback<ParseObject>() {
                    @Override
                    public void done(List<ParseObject> mdecks, ParseException e) {
                        decks = mdecks;
                        ExistingDeckAdapter adapter = new ExistingDeckAdapter(SuggestDeckActivity.this, 0, decks, purchased);
                        suggestdeck_listview.setAdapter(adapter);
                        suggestdeck_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                Boolean free = (Boolean) view.getTag();
                                if (free || purchased)
                                    getDeck(decks.get(position));
                                else {
                                    Intent intent = new Intent(SuggestDeckActivity.context, BuyDeckActivity.class);
                                    intent.putExtra("englan", language.toLowerCase());
                                    startActivity(intent);
                                }
                            }
                        });
                        loading_deck.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

    public void getDeck(ParseObject ori_deck){
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        loadingDialog = ProgressDialog.show(SuggestDeckActivity.context, null, "Loading...", true, false);
        deck = new Deck();
        deck.clone(ori_deck, language, ParseUser.getCurrentUser());
        deck.pinInBackground();
        deck.saveEventually();
        ParseQuery<ParseObject> query = ParseQuery.getQuery("SampleCard");
        query.whereEqualTo("deck", ori_deck);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> ori_cards, ParseException e) {
                for (ParseObject ori_card : ori_cards) {
                    Card card = new Card();
                    card.clone(ori_card, deck);
                    card.pinInBackground();
                    card.saveEventually();
                }
                loadingDialog.dismiss();
                SuggestDeckActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        AlertDialog.Builder builder = new AlertDialog.Builder(SuggestDeckActivity.context);
                        builder.setTitle(R.string.suggestdeck_done_title);
                        builder.setMessage(R.string.suggestdeck_done_msg);
                        builder.setPositiveButton(R.string.suggestdeck_done_okay, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        builder.create().show();
                    }
                });
            }
        });
    }


    public void back(View view){
        Intent i = new Intent(this, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

}
