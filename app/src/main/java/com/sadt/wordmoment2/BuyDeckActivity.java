package com.sadt.wordmoment2;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.vending.billing.IInAppBillingService;
import com.sadt.wordmoment2.util.IabHelper;
import com.sadt.wordmoment2.util.IabResult;
import com.sadt.wordmoment2.util.Inventory;
import com.sadt.wordmoment2.util.Purchase;

import java.util.ArrayList;
import java.util.List;

public class BuyDeckActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private IabHelper mHelper;
    private String englan;
    private Boolean purchased;
    private TextView deck_desc;
    private Button buy_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_deck);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        deck_desc = (TextView) findViewById(R.id.deck_desc);
        buy_btn = (Button) findViewById(R.id.buy_btn);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        englan = getIntent().getStringExtra("englan");

        toolbar.setTitle(dis_lan(englan));

        String base64EncodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyFHCSWbpe8X6lGJgJcJ+EW74yv5FPbNZbXbKAq7ofv4+V2lnMqR4z7LdlkYS32SWkZ5IAE3+cpWr0oEo3jGTlInm+tWbbbhK5z4ZmmwYLrmacnbVSHi8d6FlZNWbHbiDeqjWRFXfpVogV5B2LMuBPXwl4LoD34Jl+ZxF9OzkS16qNXqrY/anx21xMQUU2o2qZ7H92PtB3zuxjmNYBaS+Aqb6E/nrU/ZSnbVD2r52JzLFXvZ6saA0JOcyDyHKfyX1MRPmflhswX/lX+wlCWY2O5Kgj/T7FESIqL5SpIl14dqjstTDfZDtPxU/fzHDCMQe7kFC4mVl69/aZDi14MUZTQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d("IAP", "Problem setting up In-app Billing: " + result);
                }
                if (mHelper == null) return;

                mHelper.queryInventoryAsync(mGotInventoryListener);

                // Hooray, IAB is fully set up!
            }
        });
    }

    public void buy(View arg0) {
        String payload = "";

        mHelper.launchPurchaseFlow(this, englan, 314,
                mPurchaseFinishedListener, payload);
    }

    public String dis_lan(String englan){
        String[] languages = getResources().getStringArray(R.array.languages);
        String[] display_lan = getResources().getStringArray(R.array.display_lan);

        for (int i=0; i<languages.length; i++){
            if (englan.equals(languages[i].toLowerCase()))
                return display_lan[i];
        }
        return "";
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mHelper != null) mHelper.dispose();
        mHelper = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.buy_deck, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    IabHelper.QueryInventoryFinishedListener
            mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory)
        {
            if (result.isFailure()) {
                return;
            }

            String price = inventory.getSkuDetails(englan).getPrice();
            buy_btn.setText(getString(R.string.buydeck_buy)+" "+price);
            Log.d("Price", price);
            String desc = inventory.getSkuDetails(englan).getDescription();
            deck_desc.setText(desc);
            // update the UI
        }
    };

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d("IAP", "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                //complain("Failed to query inventory: " + result);
                return;
            }

            Log.d("IAP", "Query inventory was successful.");

            /*
             * Check for items we own. Notice that for each purchase, we check
             * the developer payload to see if it's correct! See
             * verifyDeveloperPayload().
             */

            // Do we have the premium upgrade?
            Purchase lanPurchase = inventory.getPurchase(englan);
            purchased = lanPurchase != null;
            if (purchased) {
                Log.d("IAP", "Purchased");
            }
            else{
                Log.d("IAP", "Unpurchased");
                List additionalSkuList = new ArrayList();
                additionalSkuList.add(englan);
                mHelper.queryInventoryAsync(true, additionalSkuList, mQueryFinishedListener);
            }
        }
    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d("IAP", "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                Log.d("IAP", "Error purchasing: " + result);
                //setWaitScreen(false);
                return;
            }

            Log.d("IAP", "Purchase successful.");

            if (purchase.getSku().equals(englan)) {
                Log.d("IAP", "Thank you for upgrading to premium!");
            }
        }
    };

}
