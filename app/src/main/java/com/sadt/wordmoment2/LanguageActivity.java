package com.sadt.wordmoment2;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.sadt.wordmoment2.util.IabHelper;
import com.sadt.wordmoment2.util.IabResult;
import com.sadt.wordmoment2.util.Inventory;
import com.sadt.wordmoment2.util.Purchase;

import java.util.ArrayList;
import java.util.List;


public class LanguageActivity extends ActionBarActivity {
    private LanguageAdapter adapter;
    private ListView suggestlanguage_listview;
    private ProgressBar loading_deck;
    private Toolbar toolbar;
    public static Context context;
    private IabHelper mHelper;
    List<ParseObject> languages;
    List<Boolean> purchases;
    List<String> prices;
    private List<String> additionalSkuList;
    private Boolean buymode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);

        context = this;

        buymode = getIntent().getBooleanExtra("buy", false);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        suggestlanguage_listview = (ListView) findViewById(R.id.suggestlanguage_listview);
        loading_deck = (ProgressBar) findViewById(R.id.loading_deck);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_activity_language);


        if(buymode) {
            languages = new ArrayList<>();
            prices = new ArrayList<>();
            additionalSkuList = new ArrayList<>();
            purchases = new ArrayList<>();

            ParseQuery<ParseObject> query = ParseQuery.getQuery("Language");
            query.findInBackground(new FindCallback<ParseObject>() {
                @Override
                public void done(List<ParseObject> mlanguages, ParseException e) {
                    languages = mlanguages;
                    startSetup();
                }
            });
        }
        else
            getSuggestDeck();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startSetup(){
        String base64EncodedPublicKey="MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAyFHCSWbpe8X6lGJgJcJ+EW74yv5FPbNZbXbKAq7ofv4+V2lnMqR4z7LdlkYS32SWkZ5IAE3+cpWr0oEo3jGTlInm+tWbbbhK5z4ZmmwYLrmacnbVSHi8d6FlZNWbHbiDeqjWRFXfpVogV5B2LMuBPXwl4LoD34Jl+ZxF9OzkS16qNXqrY/anx21xMQUU2o2qZ7H92PtB3zuxjmNYBaS+Aqb6E/nrU/ZSnbVD2r52JzLFXvZ6saA0JOcyDyHKfyX1MRPmflhswX/lX+wlCWY2O5Kgj/T7FESIqL5SpIl14dqjstTDfZDtPxU/fzHDCMQe7kFC4mVl69/aZDi14MUZTQIDAQAB";
        mHelper = new IabHelper(this, base64EncodedPublicKey);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    // Oh noes, there was a problem.
                    Log.d("IAP", "Problem setting up In-app Billing: " + result);
                }
                mHelper.queryInventoryAsync(mGotInventoryListener);

                // Hooray, IAB is fully set up!
            }
        });
    }

    public void getSuggestDeck(){
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                ParseQuery<ParseObject> query = ParseQuery.getQuery("Language");
                try {
                    languages = query.find();
                    adapter = new LanguageAdapter(LanguageActivity.context, 0, languages);
                    suggestlanguage_listview.post(new Runnable() {
                        @Override
                        public void run() {
                            suggestlanguage_listview.setAdapter(adapter);
                            suggestlanguage_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    Intent intent = new Intent(LanguageActivity.context, SuggestDeckActivity.class);
                                    intent.putExtra("language", languages.get(position).getString("language"));
                                    startActivityForResult(intent, 0);
                                }
                            });
                        }
                    });
                    loading_deck.post(new Runnable() {
                        @Override
                        public void run() {
                            loading_deck.setVisibility(View.GONE);
                        }
                    });
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    IabHelper.QueryInventoryFinishedListener
            mQueryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory)
        {
            if (result.isFailure()) {
                return;
            }
            for (String foreignLan : additionalSkuList){
                String price = inventory.getSkuDetails(foreignLan).getPrice();
                if (price!=null) prices.add(price);
                else prices.add("None");
                Log.d("Price", price);
            }
            adapter = new LanguageAdapter(LanguageActivity.context, 0, languages, purchases, prices);
            suggestlanguage_listview.setAdapter(adapter);
            suggestlanguage_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (buymode) {
                        Intent intent = new Intent(LanguageActivity.context, BuyDeckActivity.class);
                        intent.putExtra("englan", languages.get(position).getString("foreignLan"));
                        startActivityForResult(intent, 0);
                    } else {
                        Intent intent = new Intent(LanguageActivity.context, SuggestDeckActivity.class);
                        intent.putExtra("language", languages.get(position).getString("language"));
                        startActivityForResult(intent, 0);
                    }
                }
            });
            loading_deck.setVisibility(View.GONE);
        }
    };

    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d("IAP", "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                Log.d("IAP", "Failed to query inventory: " + result);
                return;
            }

            Log.d("IAP", "Query inventory was successful.");
            for (ParseObject language : languages){
                String foreignLan = language.getString("foreignLan");
                Purchase lanPurchase = inventory.getPurchase(foreignLan);
                Boolean purchased = lanPurchase != null;
                additionalSkuList.add(foreignLan);
                purchases.add(purchased);
            }
            mHelper.queryInventoryAsync(true, additionalSkuList, mQueryFinishedListener);
        }
    };

    /*private String deckName, foreignLan, yourLan;
    private Spinner foreign_spinner;
    private Spinner your_spinner;
    private ParseUser me;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        me = ParseUser.getCurrentUser();
        Intent intent = getIntent();
        deckName = intent.getStringExtra("deckName");
        setSpinner();
    }

    public void setSpinner() {
        foreign_spinner = (Spinner) findViewById(R.id.foreign);
        your_spinner = (Spinner) findViewById(R.id.your);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.languages, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        foreign_spinner.setAdapter(adapter);
        your_spinner.setAdapter(adapter);
    }

    public void cancel(View view) {
        Intent intent = new Intent(this, DecksActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void create(View view) {
        foreignLan = foreign_spinner.getSelectedItem().toString();
        yourLan = your_spinner.getSelectedItem().toString();
        Log.d("Deck name", deckName);
        Log.d("Foreign", foreignLan);
        Log.d("Your", yourLan);
        Deck deck = new Deck();
        deck.setDeckName(deckName);
        deck.setForeignLan(foreignLan);
        deck.setLocalLan(yourLan);
        deck.setUser(me);
        deck.setDeckId();
        deck.setLastIndex(0);
        deck.setLastUsed(new Date());
        deck.saveEventually();
        //ParsePush.subscribeInBackground(foreignLan);
        Intent intent = new Intent(this, DecksActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }*/
}
