package com.sadt.wordmoment2;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.parse.ParseFile;
import com.parse.ParseObject;

/**
 * Created by Chayanin on 15/3/2558.
 */
public class LanguageAdapter extends ArrayAdapter<ParseObject> {
    private Context context;
    private List<ParseObject> languages;
    private TextView language, price;
    private List<Boolean> purchases;
    private List<String> prices;

    public LanguageAdapter(Context context, int textViewResourceId, List<ParseObject> mLanguages){
        super(context,textViewResourceId, mLanguages);
        this.context = context;
        this.languages = mLanguages;
    }

    public LanguageAdapter(Context context, int textViewResourceId, List<ParseObject> mLanguages, List<Boolean> mpurchases, List<String> mprices){
        super(context,textViewResourceId, mLanguages);
        this.context = context;
        this.languages = mLanguages;
        this.purchases = mpurchases;
        this.prices = mprices;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = vi.inflate(R.layout.existing_language_item, parent, false);

        language = (TextView) convertView.findViewById(R.id.langauge);
        price = (TextView) convertView.findViewById(R.id.price);
        //flag = (ParseImageView) convertView.findViewById(R.id.flag);

        language.setText(dis_lan(languages.get(position).getString("language")));
        //ParseFile photoFile = languages.get(position).getParseFile("flag");
        /*if (photoFile != null) {
            Log.d("Set Flag", "Going to");
            flag.setParseFile(photoFile);
            flag.loadInBackground();
        }*/
        if(purchases==null)
            price.setVisibility(View.GONE);
        else{
            if (purchases.get(position)){
                price.setEnabled(false);
                price.setText(R.string.language_bought);
            }else{
                price.setText(prices.get(position));
            }
        }

        return convertView;
    }

    public String dis_lan(String eng){
        String[] languegs = LanguageActivity.context.getResources().getStringArray(R.array.languages);
        String[] dis_lans = LanguageActivity.context.getResources().getStringArray(R.array.display_lan);

        for (int i=0; i<languegs.length; i++){
            if (eng.equals(languegs[i]))
                return dis_lans[i];
        }
        return "";
    }
}
