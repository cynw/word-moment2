package com.sadt.wordmoment2;

import android.os.AsyncTask;
import android.util.Log;

import com.sadt.wordmoment2.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

/**
 * Created by Chayanin on 6/3/2558.
 */
public class LoadImage extends AsyncTask<Void, Void, Void> {
    private String serverUrl = "https://api.datamarket.azure.com/Bing/Search/v1/Image";
    private String accountKey = "YscIdEyiqN622QVbw2TOFy+8z4LKs/6XYYdRsCZpUSk";
    private ArrayList<String> urls;
    private String word;
    private String res;
    private Error mError;

    public LoadImage(String mWord) {
        word = mWord;
        urls = new ArrayList<String>();
    }

    @Override
    protected Void doInBackground(Void... params) {
        try {
            serverUrl += "?Query=%27";
            serverUrl += word;
            serverUrl += "%27&ImageFilters=%27Size%3AMedium%27&$format=json";
            /*byte[] accountKeyBytes = Base64.encodeBase64((accountKey + ":" + accountKey).getBytes());
            String accountKeyEnc = new String(accountKeyBytes);
            URL url = null;
            url = new URL(serverUrl);

            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Authorization", "Basic " + accountKeyEnc);
            InputStream response = urlConnection.getInputStream();
            res = readStream(response);
            Log.d("Res", res);*/
        } catch (Exception e) {
            e.printStackTrace();
            mError = new Error(e.getMessage(), e);
            //Log.e(TAG, e.getMessage());
        }

        try {
            JSONObject jObject = new JSONObject(res);
            if (jObject.has("d")) {
                Log.d("Add", "A");
                JSONObject d = jObject.getJSONObject("d");
                if (d.has("results")) {
                    Log.d("Add", "B");
                    JSONArray results = d.getJSONArray("results");
                    for (int i = 0; i < results.length(); i++) {
                        if (results.getJSONObject(i).has("MediaUrl")) {
                            Log.d("Add", "C");
                            urls.add(results.getJSONObject(i).getString("MediaUrl"));
                            if (i == 3)
                                break;
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("url size", String.valueOf(urls.size()));
        for (int i = 0; i < urls.size(); i++)
            Log.d("url", urls.get(i));
        /*new LoadImage2(urls.get(0), PlayingActivity.image1, PlayingActivity.progress1).execute();
        new LoadImage2(urls.get(1), PlayingActivity.image2, PlayingActivity.progress2).execute();
        new LoadImage2(urls.get(2), PlayingActivity.image3, PlayingActivity.progress3).execute();
        new LoadImage2(urls.get(3), PlayingActivity.image4, PlayingActivity.progress4).execute();*/
        return null;
    }

    private String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder sb = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                //System.out.println(line);
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
