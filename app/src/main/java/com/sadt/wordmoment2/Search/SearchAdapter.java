package com.sadt.wordmoment2.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.R;

import java.util.List;

/**
 * Created by CWONG on 2015-08-24.
 */
public class SearchAdapter extends ArrayAdapter<Card> {
    private Context context;
    private String foreign, local;
    private List<Card> cards;
    private ViewHolder holder = null;

    public SearchAdapter(Context context, int textViewResourceId, List<Card> mCards) {
        super(context, textViewResourceId, mCards);
        this.context = context;
        this.cards = mCards;
    }

    public class ViewHolder {
        private TextView deckname_tv, deckname2_tv, deckprice_tv;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.existing_deck_item, null);

            holder = new ViewHolder();

            holder.deckname_tv = (TextView) convertView.findViewById(R.id.deckname_tv);
            holder.deckname2_tv = (TextView) convertView.findViewById(R.id.deckname2_tv);
            holder.deckprice_tv = (TextView) convertView.findViewById(R.id.deckprice_tv);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        foreign = cards.get(position).getForeign();
        local = cards.get(position).getLocal();

        holder.deckname_tv.setText(foreign);
        holder.deckname2_tv.setText(local);
        holder.deckprice_tv.setText(R.string.search_card);

        return convertView;
    }

    public void addAll(List<Card> mcards){
        this.cards.addAll(mcards);
        notifyDataSetChanged();
    }

    public void clear(){
        this.cards.clear();
    }
}
