package com.sadt.wordmoment2.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;

import java.util.List;

/**
 * Created by CWONG on 2015-08-26.
 */
public class SearchAdapter2 extends ArrayAdapter<Deck> {
    private Context context;
    private String deckName, deckName2;
    private List<Deck> decks;
    private ViewHolder holder = null;

    public SearchAdapter2(Context context, int textViewResourceId, List<Deck> mDecks) {
        super(context, textViewResourceId, mDecks);
        this.context = context;
        this.decks = mDecks;
    }

    public class ViewHolder {
        private TextView deckname_tv, deckname2_tv, deckprice_tv;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.existing_deck_item, null);

            holder = new ViewHolder();

            holder.deckname_tv = (TextView) convertView.findViewById(R.id.deckname_tv);
            holder.deckname2_tv = (TextView) convertView.findViewById(R.id.deckname2_tv);
            holder.deckprice_tv = (TextView) convertView.findViewById(R.id.deckprice_tv);
            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder) convertView.getTag();
        }

        deckName = decks.get(position).getDeckName();
        deckName2 = decks.get(position).getDeckName2();

        holder.deckname_tv.setText(deckName);
        holder.deckname2_tv.setText(deckName2);
        holder.deckprice_tv.setText(R.string.search_deck);

        return convertView;
    }

    public void addAll(List<Deck> mDecks){
        this.decks.addAll(mDecks);
        notifyDataSetChanged();
    }

    public void clear(){
        this.decks.clear();
    }

    public List<Deck> getDecks(){
        return this.decks;
    }
}
