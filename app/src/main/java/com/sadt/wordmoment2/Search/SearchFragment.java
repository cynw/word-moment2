package com.sadt.wordmoment2.Search;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.sadt.wordmoment2.Deck.DeckActivity;
import com.sadt.wordmoment2.MainActivity;
import com.sadt.wordmoment2.Object.Card;
import com.sadt.wordmoment2.Object.Deck;
import com.sadt.wordmoment2.R;
import com.sadt.wordmoment2.Search.SearchAdapter;
import com.sadt.wordmoment2.Search.SearchAdapter2;

import java.util.ArrayList;
import java.util.List;


public class SearchFragment extends Fragment {
    private EditText search_et;
    private Button cancel_btn;
    private ListView search_decks_lv, search_cards_lv;
    private LinearLayout search_desc_lo;
    private SearchAdapter adapter;
    private SearchAdapter2 adapter2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_search, container, false);

        search_et = (EditText) rootView.findViewById(R.id.search_et);
        cancel_btn = (Button) rootView.findViewById(R.id.cancel_btn);
        search_decks_lv = (ListView) rootView.findViewById(R.id.search_decks_lv);
        search_cards_lv = (ListView) rootView.findViewById(R.id.search_cards_lv);
        search_desc_lo = (LinearLayout) rootView.findViewById(R.id.search_desc_lo);


        adapter = new SearchAdapter(MainActivity.context, 0, new ArrayList<Card>());
        search_cards_lv.setAdapter(adapter);

        adapter2 = new SearchAdapter2(MainActivity.context, 0, new ArrayList<Deck>());
        search_decks_lv.setAdapter(adapter2);
        search_decks_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(MainActivity.context, DeckActivity.class);
                i.putExtra("deckId", adapter2.getDecks().get(position).getDeckId());
                startActivity(i);
            }
        });

        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search_et.setText("");
                cancel_btn.setVisibility(View.GONE);
            }
        });

        search_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.d("Search", "Key listener");
                adapter.clear();
                adapter2.clear();
                String query = search_et.getText().toString();
                if (query == null || query.trim().equals("")) {
                    cancel_btn.setVisibility(View.GONE);
                    search_desc_lo.setVisibility(View.VISIBLE);
                    search_decks_lv.setVisibility(View.INVISIBLE);
                    search_cards_lv.setVisibility(View.INVISIBLE);
                } else {
                    cancel_btn.setVisibility(View.VISIBLE);
                    search_desc_lo.setVisibility(View.GONE);
                    search_decks_lv.setVisibility(View.VISIBLE);
                    search_cards_lv.setVisibility(View.VISIBLE);
                    update(query);
                }
            }
        });

        return rootView;
    }

    public void update(String query){
        Log.d("Search", "Updating" + query);
        ParseQuery<Deck> deckName = Deck.getQuery();
        deckName.whereStartsWith("deckName", query);
        deckName.fromLocalDatastore();

        ParseQuery<Deck> deckName2 = Deck.getQuery();
        deckName2.whereStartsWith("deckName2", query);
        deckName2.fromLocalDatastore();

        List<ParseQuery<Deck>> queries1 = new ArrayList<ParseQuery<Deck>>();
        queries1.add(deckName);
        queries1.add(deckName2);

        ParseQuery<Deck> mainQuery1 = ParseQuery.or(queries1);
        mainQuery1.findInBackground(new FindCallback<Deck>() {
            @Override
            public void done(List<Deck> decks, ParseException e) {
                if (e == null) {
                    adapter2.addAll(decks);
                    Log.d("Search", "Adding decks"+ String.valueOf(decks.size()));
                } else Log.d("Search", e.getMessage());
            }
        });

        ParseQuery<Card> cardLocal = Card.getQuery();
        cardLocal.whereStartsWith("local", query);
        cardLocal.fromLocalDatastore();

        ParseQuery<Card> cardForeign = Card.getQuery();
        cardForeign.whereStartsWith("foreign", query);
        cardForeign.fromLocalDatastore();

        List<ParseQuery<Card>> queries2 = new ArrayList<ParseQuery<Card>>();
        queries2.add(cardLocal);
        queries2.add(cardForeign);

        ParseQuery<Card> mainQuery2 = ParseQuery.or(queries2);
        mainQuery2.findInBackground(new FindCallback<Card>() {
            @Override
            public void done(List<Card> cards, ParseException e) {
                if (e == null) {
                    adapter.addAll(cards);
                    Log.d("Search", "Adding cards" + String.valueOf(cards.size()));
                } else Log.d("Search", e.getMessage());
            }
        });
    }

}
