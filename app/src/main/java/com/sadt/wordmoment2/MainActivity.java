package com.sadt.wordmoment2;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sadt.wordmoment2.Decks.AddDeckActivity;
import com.sadt.wordmoment2.Decks.DecksFragment;
import com.sadt.wordmoment2.Decks.ManageDecksActivity;
import com.sadt.wordmoment2.Main.ProfileFragment;
import com.sadt.wordmoment2.Search.SearchFragment;
import com.sadt.wordmoment2.Settings.SettingsFragment;


public class MainActivity extends AppCompatActivity {

    // Declaring Your View and Variables

    private Toolbar toolbar;
    /*private ViewPager pager;
    private ViewPagerAdapter adapter;
    private SlidingTabLayout tabs;*/
    public static Context context;
    private TextView home_tv, profile_tv,search_tv, settings_tv;
    private ImageView home_iv, profile_iv,search_iv, settings_iv;
    /*private CharSequence Titles[]={"1","2","3","4"};
    private int Numboftabs =4;*/
    private int current;
    MenuItem add_menu, edit_menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        // Creating The Toolbar and setting it as the Toolbar for the activity

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        home_iv = (ImageView) findViewById(R.id.home_iv);
        search_iv = (ImageView) findViewById(R.id.search_iv);
        profile_iv = (ImageView) findViewById(R.id.profile_iv);
        settings_iv = (ImageView) findViewById(R.id.settings_iv);
        home_tv = (TextView) findViewById(R.id.home_tv);
        search_tv = (TextView) findViewById(R.id.search_tv);
        profile_tv = (TextView) findViewById(R.id.profile_tv);
        settings_tv = (TextView) findViewById(R.id.settings_tv);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        DecksFragment fragment = new DecksFragment();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        fragmentTransaction.commit();

        current = 0;


        /*// Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPagerAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        add_menu = menu.findItem(R.id.add);
        edit_menu = menu.findItem(R.id.edit);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.context);
                builder.setTitle(R.string.add_deck_dialog_title)
                        .setItems(R.array.add_deck_dialog_items, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                if (which == 0) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(MainActivity.context, AddDeckActivity.class);
                                    startActivity(intent);
                                }
                                else if (which == 1) {
                                    dialog.dismiss();
                                    Intent intent = new Intent(MainActivity.context, LoadDeckActivity.class);
                                    intent.putExtra("deckId", "none");
                                    startActivity(intent);
                                }
                                else if (which ==2){
                                    dialog.dismiss();
                                    Intent intent = new Intent(MainActivity.context, LanguageActivity.class);
                                    intent.putExtra("buy", false);
                                    startActivity(intent);
                                }
                            }
                        });
                AlertDialog dialog = builder.create();
                dialog.show();
                return super.onOptionsItemSelected(item);
            case R.id.edit:
                Intent intent = new Intent(this, ManageDecksActivity.class);
                startActivity(intent);
                return super.onOptionsItemSelected(item);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void init(){
        home_iv.setImageResource(R.drawable.home_u);
        search_iv.setImageResource(R.drawable.search_u);
        profile_iv.setImageResource(R.drawable.profile_u);
        settings_iv.setImageResource(R.drawable.settings_u);

        home_tv.setTextColor(getResources().getColor(R.color.tabs_u_text));
        search_tv.setTextColor(getResources().getColor(R.color.tabs_u_text));
        profile_tv.setTextColor(getResources().getColor(R.color.tabs_u_text));
        settings_tv.setTextColor(getResources().getColor(R.color.tabs_u_text));
    }

    public void home(View v){
        if(current != 0) {
            init();
            home_iv.setImageResource(R.drawable.home_a);
            home_tv.setTextColor(getResources().getColor(R.color.tabs_a_text));
            DecksFragment fragment = new DecksFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide3, R.anim.slide4);
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
            current = 0;
            add_menu.setVisible(true);
            edit_menu.setVisible(true);
        }
    }

    public void search(View v){
        if(current != 1) {
            init();
            search_iv.setImageResource(R.drawable.search_a);
            search_tv.setTextColor(getResources().getColor(R.color.tabs_a_text));
            SearchFragment fragment = new SearchFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            if(current <1 )fragmentTransaction.setCustomAnimations(R.anim.slide1, R.anim.slide2);
            else fragmentTransaction.setCustomAnimations(R.anim.slide3, R.anim.slide4);
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
            current = 1;
            add_menu.setVisible(false);
            edit_menu.setVisible(false);
        }
    }

    public void profile(View v){
        if(current != 2) {
            init();
            profile_iv.setImageResource(R.drawable.profile_a);
            profile_tv.setTextColor(getResources().getColor(R.color.tabs_a_text));
            ProfileFragment fragment = new ProfileFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            if(current <2 )fragmentTransaction.setCustomAnimations(R.anim.slide1, R.anim.slide2);
            else fragmentTransaction.setCustomAnimations(R.anim.slide3, R.anim.slide4);
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
            current = 2;
            add_menu.setVisible(false);
            edit_menu.setVisible(false);
        }
    }

    public void settings(View v){
        if(current != 3) {
            init();
            settings_iv.setImageResource(R.drawable.settings_a);
            settings_tv.setTextColor(getResources().getColor(R.color.tabs_a_text));
            SettingsFragment fragment = new SettingsFragment();
            FragmentManager fm = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fm.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.slide1, R.anim.slide2);
            fragmentTransaction.replace(R.id.fragment_container, fragment);
            fragmentTransaction.commit();
            current = 3;
            add_menu.setVisible(false);
            edit_menu.setVisible(false);
        }
    }
}