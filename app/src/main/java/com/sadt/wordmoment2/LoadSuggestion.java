package com.sadt.wordmoment2;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.sadt.wordmoment2.Deck.AddWordActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Chayanin on 2015-01-30.
 */
public class LoadSuggestion extends AsyncTask<String, Integer, String> {

    private String from, to, word;
    private String serverUrl = "https://glosbe.com/gapi/translate?";
    private String from_code, to_code;
    private String mode;

    @Override
    protected String doInBackground(String... params) {
        int TIMEOUT_MILLISEC = 10000;  // = 10 seconds
        from = params[0];
        to = params[1];
        word = params[2].toLowerCase();
        mode = params[3];
        from_code = lan_code(from);
        to_code = lan_code(to);
        StringBuilder builder = new StringBuilder();
        HttpParams httpParams = new BasicHttpParams();
        HttpConnectionParams.setConnectionTimeout(httpParams, TIMEOUT_MILLISEC);
        HttpConnectionParams.setSoTimeout(httpParams, TIMEOUT_MILLISEC);
        HttpClient client = new DefaultHttpClient(httpParams);
        try {
            serverUrl += "from=" + from_code;
            serverUrl += "&dest=" + to_code;
            serverUrl += "&format=json";
            serverUrl += "&phrase=" + URLEncoder.encode(word, "UTF-8");
            serverUrl += "&pretty=true";
            Log.d("Server URL", serverUrl);
            HttpGet request = new HttpGet(serverUrl);
            HttpResponse response = client.execute(request);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
            } else {
                Log.e("Wrong", "Failed to download file");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return builder.toString();
    }

    // This is called when doInBackground() is finished
    protected void onPostExecute(String result) {
        ArrayList<String> trans = new ArrayList<>();
        try {
            Log.d("Result", result);
            JSONObject jObject = new JSONObject(result);
            if (jObject.has("tuc")) {
                JSONArray tuc = jObject.getJSONArray("tuc");
                Log.d("tuc", tuc.toString());
                for (int i = 0; i < tuc.length(); i++) {
                    if (tuc.getJSONObject(i).has("meanings")) {
                        JSONArray meanings = tuc.getJSONObject(i).getJSONArray("meanings");
                        Log.d("meanings", meanings.toString());
                        for (int j = 0; j < meanings.length(); j++) {
                            Log.d("Meaning", meanings.getJSONObject(j).getString("text"));
                            trans.add(meanings.getJSONObject(j).getString("text"));
                        }
                    }
                    if (tuc.getJSONObject(i).has("phrase")) {
                        Log.d("Phrase", tuc.getJSONObject(i).getJSONObject("phrase").getString("text"));
                        trans.add(tuc.getJSONObject(i).getJSONObject("phrase").getString("text"));
                    }
                }
            }
            for (int i = 0; i < trans.size(); i++)
                Log.d("Trans", trans.get(i));
            Log.d("JObject", jObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AddWordActivity.progressBar.setVisibility(View.GONE);
        final String[] show_array = trans.toArray(new String[trans.size()]);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddWordActivity.context, android.R.layout.simple_list_item_1, show_array);
        adapter.notifyDataSetChanged();
        AddWordActivity.suggestion_list.setAdapter(adapter);
        AddWordActivity.suggestion_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String str = show_array[position];
                str = str.substring(0, 1).toUpperCase() + str.substring(1);
                if (mode.equals("fromfor"))
                    AddWordActivity.meaning_et.setText(str);
                else
                    AddWordActivity.word_et.setText(str);
            }
        });
        AddWordActivity.suggestion_list.setVisibility(View.VISIBLE);
    }

    public String lan_code(String eng){
        String[] languegs = AddWordActivity.context.getResources().getStringArray(R.array.languages);
        String[] lancodes = AddWordActivity.context.getResources().getStringArray(R.array.lan_code);

        for (int i=0; i<languegs.length; i++){
            if (eng.equals(languegs[i]))
                return lancodes[i];
        }
        return "";
    }
}
