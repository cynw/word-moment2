package com.sadt.wordmoment2.Object;

import java.io.Serializable;
import java.util.Date;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseQuery;

/**
 * Created by Chayanin on 2015-01-20.
 */
@ParseClassName("Card")
public class Card extends ParseObject implements Serializable{
    public String getForeign() {
        return getString("foreign");
    }

    public void setForeign(String word) {
        put("foreign", word);
    }

    public String getLocal() {
        return getString("local");
    }
    
    public Deck getDeck() {
        return (Deck) getParseObject("deck");
    }

    public void setLocal(String word) {
        put("local", word);
    }

    public void setDeck(Deck deck) {
        put("deck", deck);
    }

    public void setDisplay(Date date) {
        put("display", date);
    }

    public void clone(Card mcard, Deck mdeck) {
        setForeign(mcard.getForeign());
        setLocal(mcard.getLocal());
        setDeck(mcard.getDeck());
        setDisplay(new Date());
        setDeck(mdeck);
    }

    public void clone(ParseObject ori_card, Deck deck) {
        setDeck(deck);
        setDisplay(new Date());
        setForeign(ori_card.getString("foreign"));
        setLocal(ori_card.getString("local"));
    }
    
    public static ParseQuery<Card> getQuery() {
        return ParseQuery.getQuery(Card.class);
    }

}
