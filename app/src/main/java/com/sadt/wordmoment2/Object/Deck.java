package com.sadt.wordmoment2.Object;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;

import android.util.Log;

import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

/**
 * Created by Chayanin on 2015-01-20.
 */
@ParseClassName("Deck")
public class Deck extends ParseObject implements Serializable{
	
	public Deck(){
		
	}
	
	public Deck(String name,String name2, ParseUser user, String foreign, String local){
		setDeckName(name);
		setDeckName2(name2);
		setUser(user);
		setForeignLan(foreign);
		setLocalLan(local);
		setDeckId();
		setLastIndex(0);
		setLastUsed(new Date());
	}

    public static ParseQuery<Deck> getQuery() {
        return ParseQuery.getQuery(Deck.class);
    }

    public int getDeckId() {
        return getInt("deckId");
    }

    public String getDeckName() {
        return getString("deckName");
    }

    public void setDeckName2(String deckName2) {
        put("deckName2", deckName2);
    }
    
    public String getDeckName2() {
        return getString("deckName2");
    }

    public void setDeckName(String deckName) {
        put("deckName", deckName);
    }

    public void setUser(ParseUser user) {
        put("user", user);
    }

    public String getForeignLan() {
        return getString("foreignLan");
    }

    public void setForeignLan(String foreignLan) {
        put("foreignLan", foreignLan);
    }

    public String getLocalLan() {
        return getString("LocalLan");
    }

    public void setLocalLan(String LocalLan) {
        put("LocalLan", LocalLan);
    }

    public void setDeckId() {
        while (true) {
            Random rand = new Random();
            int deckId = rand.nextInt(100000);
            ParseQuery<Deck> query = ParseQuery.getQuery(Deck.class);
            query.whereEqualTo("deckId", deckId);
            List<Deck> result = null;
            try {
                result = query.find();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if (result.size() == 0) {
                put("deckId", deckId);
                Log.d("Randomized deck Id", String.valueOf(deckId));
                return;
            }
        }
    }

    public int getLastIndex() {
        return getInt("lastIndex");
    }

    public void setLastIndex(int index) {
        put("lastIndex", index);
    }

    public Date getLastUsed() {
        return getDate("lastUsed");
    }

    public void clone(Deck deck, ParseUser user){
        setDeckId();
        setDeckName(deck.getDeckName());
        setDeckName2(deck.getDeckName2());
        setLocalLan(deck.getLocalLan());
        setForeignLan(deck.getForeignLan());
        setLastIndex(0);
        setLastUsed(new Date());
        setUser(user);
    }

    public void clone(ParseObject ori_deck, String forlan, ParseUser user){
        setDeckId();
        setLastIndex(0);
        setLocalLan("English");
        setForeignLan(forlan);
        setUser(user);
        setDeckName(ori_deck.getString("deckName"));
        setDeckName2(ori_deck.getString("deckName2"));
        setLastUsed(new Date());
    }

    public void setLastUsed(Date date) {
        put("lastUsed", date);
    }
}
